<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OrderExport implements FromView, ShouldAutoSize, WithStyles
{
    use Exportable;

    public function __construct(Array $order) {
        $this->order = $order;
    }

    /* public function columnWidths(): array
    {
        return [
            'B' => 100,
            'C' => 50,
        ];
    } */

    public function styles(Worksheet $sheet)
    {
        $lastRow = sizeof($this->order)+2;
        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $sheet->getStyle('B2:K'.$lastRow)->applyFromArray($styleArray);
        $sheet->getStyle('A:K')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $sheet->getStyle('A:K')->getAlignment()->setWrapText(true);
    }

    public function view() : View
    {
        return view('order', [
            'order' => $this->order
        ]);
    }
}
