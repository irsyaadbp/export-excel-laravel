<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function order_item_meta()
    {
        return $this->hasMany(OrderItemMeta::class);
    }

    public function custom_photo()
    {
        return $this->hasMany(OrderItemMeta::class);
    }

    public function order_item_variants()
    {
        return $this->hasMany(OrderItemVariant::class);
    }
}
