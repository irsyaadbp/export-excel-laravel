<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function pre_order()
    {
        return $this->belongsTo(PreOrder::class);
    }

    public function cart_metas()
    {
        return $this->hasMany(CartMeta::class);
    }
}
