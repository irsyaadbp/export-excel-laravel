<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreOrder extends Model
{
    use HasFactory;
    protected $guarded = ['id'];


    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function pre_order_metas()
    {
        return $this->hasMany(PreOrderMeta::class);
    }
}
