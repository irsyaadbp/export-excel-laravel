<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function user_detail()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function payment_method()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }

    public function history()
    {
        return $this->hasMany(OrderHistory::class);
    }

    public function order_item()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function order_meta()
    {
        return $this->hasMany(OrderMeta::class);
    }
}
