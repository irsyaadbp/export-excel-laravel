<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MustAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if (!Auth::check()) {
            return response()->json(['success' => false, 'message' => 'Please login first', 'error' => 'Unauthorized'], 401);
        }

        if (Auth::check() && !$this->isAdmin($request)) {
            return response()->json(['success' => false, 'message' => 'You are not allowed to access', 'error' => 'Unauthorized'], 401);
        }

        return $next($request);
    }

    private function isAdmin(Request $request)
    {
        return $request->user()->role === 'admin';
    }
}
