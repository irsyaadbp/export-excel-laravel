<?php

namespace App\Http\Controllers;

use App\Models\ProductVariant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class VariantAdminController extends Controller
{
    public function list(Request $request)
    {
        try {
            // // Pagination
            $page = $request->get('page', 1);
            $per_page = $request->get('per_page', 10);
            $offset = $per_page * ($page - 1);

            // Search
            $search = $request->get('search', '');

            // Sorting
            $sort = $request->get('sort', 'updated_at');
            $order = $request->get('order', 'desc');

            $total = ProductVariant::select('count(id)')->where('name', 'ilike', '%' . $search . '%')->count();

            $variants = ProductVariant::where('name', 'ilike', '%' . $search . '%')->orderBy($sort, $order)->skip($offset)
                ->take($per_page)
                ->get();

            $response = array(
                "success" => true,
                "total" => (int) $total,
                "page" => (int) $page,
                "per_page" => (int) $per_page,
                "data" => $variants
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();

        try {
            $data = $request->only('name', 'description');

            $validator = Validator::make($data, [
                'name' => 'required|unique:product_variants|max:255',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }
            DB::table('product_variants')->insert($data);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success create variant'], 201);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request, ProductVariant $variant)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('name', 'description');
            $id = $variant->id;

            $validator = Validator::make($data, [
                'name' => ["max:255", Rule::unique('product_variants')->ignore($id)],
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }


            DB::table('product_variants')->where('id', $id)->update($data);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update variant']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function delete(Request $request, ProductVariant $variant)
    {
        DB::beginTransaction();

        try {

            DB::table('product_variants')->delete($variant->id);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success delete variant']);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}
