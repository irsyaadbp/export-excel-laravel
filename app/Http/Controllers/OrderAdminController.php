<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Exports\OrderExport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

class OrderAdminController extends Controller
{
    public function list(Request $request)
    {
        try {
            // Pagination
            $page = $request->get('page', 1);
            $per_page = $request->get('per_page', 10);
            $offset = $per_page * ($page - 1);

            // Search
            $search = $request->get('search', '');
            $customer = $request->get('customer', '');
            $delivery_type = $request->get('delivery_type', '');
            $status = $request->get('status', '');

            $order_date = $request->get('order_date', '');

            $order_date_start = $request->get('order_date_start', '');
            $order_date_end = $request->get('order_date_end', '');

            $created_date_start = $request->get('created_date_start', '');
            $created_date_end = $request->get('created_date_end', '');

            // Sorting
            $sort = $request->get('sort', 'updated_at');
            $order = $request->get('order', 'desc');

            $order_list = Order::with(['user_detail', 'order_item' => function ($product) {
                $product->with(['order_item_meta' => function ($meta) {
                    $meta->where('meta_key', 'greeting_card');
                }, 'order_item_variants', 'custom_photo' => function ($meta) {
                    $meta->where('meta_key', 'custom_photo');
                }])->get();
            }])->whereHas('user_detail', function ($query) use ($customer) {
                $query->where('first_name', 'LIKE', '%' . $customer . '%')
                    ->orWhere('last_name', 'LIKE', '%' . $customer . '%')
                    ->orWhere('phone', 'LIKE', '%' . $customer . '%')
                    ->orWhere('email', 'LIKE', '%' . $customer . '%');
            })
                ->where('order_number', 'LIKE', '%' . $search . '%')
                ->when($delivery_type, function ($q) use ($delivery_type) {
                    return $q->where('delivery_type', $delivery_type);
                })
                ->when($status, function ($q) use ($status) {
                    return $q->where('status', $status);
                });

            if (($request->has('order_date') && isset($order_date))) {
                $order_list
                    ->whereDate('order_date', '=', $order_date);
            }


            if (($request->has('order_date_start') && isset($order_date_start)) && ($request->has('order_date_end') && isset($order_date_end))) {
                $order_list
                    ->whereDate('order_date', '>=', $order_date_start)
                    ->whereDate('order_date', '<=', $order_date_start);
            }

            if (($request->has('created_date_start') && isset($created_date_start)) && ($request->has('created_date_end') && isset($created_date_end))) {
                $order_list
                    ->whereDate('created_at', '>=', $created_date_start)
                    ->whereDate('created_at', '<=', $created_date_end);
            }

            $order_list->orderBy($sort, $order);

            $response = array(
                "success" => true,
                "total" => (int) $order_list->count(),
                "page" => (int) $page,
                "per_page" => (int) $per_page,
                "data" => $order_list
                    ->skip($offset)
                    ->take($per_page)
                    ->get()
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function getDetail(Request $request)
    {
        try {
            $orderNumber = $request->get('orderNumber', '');

            if (!$orderNumber) {
                return response()->json(['success' => false, "message" => "Order Number can't be empty"], 422);
            }

            $selected_order = Order::with(['order_item' => function ($product) {
                $product->with(['order_item_meta', 'order_item_variants'])->get();
            }, 'order_meta', 'history' => function ($history) {
                $history->with(['user_detail'])->orderBy('id', 'asc')->get();
            }, 'payment_method'])->where('order_number', $orderNumber)->first();

            if (isset($selected_order)) {
                return response()->json(['success' => true, 'data' => $selected_order]);
            }
            return response()->json(['success' => true, 'message' => "Order number not found"], 404);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function updateStatus(Request $request, Order $order)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('status', 'note');
            $id = $order->id;

            $validator = Validator::make($data, [
                'status' => "required|in:process,ready_pickup,delivery,done,reject,waiting_payment",
                'note' => "max:255",
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            DB::table('orders')->where('id', $id)->update(['status' => $data['status']]);

            $user = Auth::user();

            switch ($data['status']) {
                case 'process':
                    DB::table('order_histories')->insert([
                        [
                            'order_id' => $id,
                            'activity' => "Pembayaran berhasil dikonfirmasi",
                            'description' => $data['note'],
                            'custom_color' => "success",
                            'update_by' => $user->id
                        ],
                        [
                            'order_id' => $id,
                            'activity' => "Order mu sedang dalam proses pembuatan",
                            'description' => "",
                            'custom_color' => "info",
                            'update_by' => $user->id
                        ]
                    ]);
                    break;
                case 'ready_pickup':
                    DB::table('order_histories')->insert([
                        [
                            'order_id' => $id,
                            'activity' => "Order mu sudah siap untuk diambil",
                            'description' => $data['note'],
                            'custom_color' => "info",
                            'update_by' => $user->id
                        ]
                    ]);
                    break;
                case 'delivery':
                    DB::table('order_histories')->insert([
                        [
                            'order_id' => $id,
                            'activity' => "Order mu sedang dalam perjalanan ke tempatmu",
                            'description' => $data['note'],
                            'custom_color' => "info",
                            'update_by' => $user->id
                        ]
                    ]);
                    break;
                case 'done':
                    DB::table('order_histories')->insert([
                        [
                            'order_id' => $id,
                            'activity' => "Order mu sudah sampai, selamat menikmati!",
                            'description' => $data['note'],
                            'custom_color' => "success",
                            'update_by' => $user->id
                        ]
                    ]);
                    break;
                case 'reject':
                    $bukti = DB::table('order_metas')->where('meta_key', 'bukti_transfer_photo')->where('order_id', $id)->get();

                    DB::table('order_metas')->where('meta_key', 'bukti_transfer_photo')->where('order_id', $id)->delete();
                    DB::table('order_metas')->where('meta_key', 'bukti_transfer_note')->where('order_id', $id)->delete();

                    DB::table('order_histories')->insert([
                        [
                            'order_id' => $id,
                            'activity' => "Ordermu dibatalkan oleh admin!",
                            'description' => $data['note'],
                            'custom_color' => "danger",
                            'update_by' => $user->id
                        ],
                    ]);

                    if (isset($bukti->meta_value) && file_exists("images/orders/{$id}" . $bukti->meta_value)) {
                        Storage::delete("images/orders/{$id}" . $bukti->meta_value);
                    }
                    break;
                case 'waiting_payment':
                    $bukti = DB::table('order_metas')->where('meta_key', 'bukti_transfer_photo')->where('order_id', $id)->get();

                    DB::table('order_metas')->where('meta_key', 'bukti_transfer_photo')->where('order_id', $id)->delete();
                    DB::table('order_metas')->where('meta_key', 'bukti_transfer_note')->where('order_id', $id)->delete();

                    DB::table('order_histories')->insert([
                        [
                            'order_id' => $id,
                            'activity' => "Pembayaranmu telah ditolak oleh admin!",
                            'description' => $data['note'],
                            'custom_color' => "danger",
                            'update_by' => $user->id
                        ],
                        [
                            'order_id' => $id,
                            'activity' => "Mohon segera melakukan pembayaran ulang!",
                            'description' => null,
                            'custom_color' => "warning",
                            'update_by' => null
                        ]
                    ]);

                    if (isset($bukti->meta_value) && file_exists("images/orders/{$id}" . $bukti->meta_value)) {
                        Storage::delete("images/orders/{$id}" . $bukti->meta_value);
                    }
                    break;

                default:
                    break;
            }

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update order status']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function downloadAllImage(Request $request, Order $order)
    {
        try {
            $orderNumber = $order->order_number;
            $order_number = str_replace('#', '', $orderNumber);
            $path = "storage/images/orders/{$order_number}";
            $fileName = "{$path}/image-{$orderNumber}.zip";
            $this->generateZip($path, $fileName);

            return response()->download(public_path($fileName))->deleteFileAfterSend(true);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function deleteAllImage(Request $request, Order $order)
    {
        DB::beginTransaction();
        try {
            $order_id = $order->id;
            $orderNumber = $order->order_number;

            $orderItemIds = DB::table('order_items')->select('id', 'product_name')->where('order_id', $order_id)->get();

            foreach ($orderItemIds as $item) {
                $id = $item->id;
                DB::table('order_item_metas')->where('order_item_id', $id)->where('meta_key', 'custom_photo')->delete();
                DB::table('order_item_metas')->where('order_item_id', $id)->insert(['order_item_id' => $id, 'meta_key' => 'is_image_deleted', 'meta_value' => 1]);
                $product_name = str_replace(' ', '-', $item->product_name) . '_' . $id;
                $order_number = str_replace('#', '', $orderNumber);
                $path = "images/orders/{$order_number}/{$product_name}";
                if (Storage::exists($path)) Storage::deleteDirectory($path);
            }

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success delete image']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    function generateZip($source, $destination)
    {
        if (is_string($source)) $source_arr = array($source); // convert it to array

        if (!extension_loaded('zip')) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        foreach ($source_arr as $source) {
            if (!file_exists($source)) continue;
            $source = str_replace('\\', '/', realpath($source));

            if (is_dir($source) === true) {
                $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

                foreach ($files as $file) {
                    $file = str_replace('\\', '/', realpath($file));

                    if (is_dir($file) === true) {
                        $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                    } else if (is_file($file) === true) {
                        $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                    }
                }
            } else if (is_file($source) === true) {
                $zip->addFromString(basename($source), file_get_contents($source));
            }
        }

        return $zip->close();
    }

    public function downloadExcel(Request $request)
    {
        $order = $this->list($request);
        $orderData = $order->getData();
        
        return (new OrderExport($orderData->data))->download('report.xlsx');
    }
}
