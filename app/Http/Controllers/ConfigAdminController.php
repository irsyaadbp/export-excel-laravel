<?php

namespace App\Http\Controllers;

use App\Models\Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ConfigAdminController extends Controller
{
    public function list(Request $request)
    {
        try {
            // // Pagination
            $page = $request->get('page', 1);
            $per_page = $request->get('per_page', 10);
            $offset = $per_page * ($page - 1);

            // Search
            $search = $request->get('search', '');

            // Sorting
            $sort = $request->get('sort', 'updated_at');
            $order = $request->get('order', 'desc');

            $total = Config::select('count(id)')->where('meta_key', 'ilike', $search . '%')->count();

            $products = Config::where('meta_key', 'ilike', $search . '%')->orderBy($sort, $order)->skip($offset)
                ->take($per_page)
                ->get();

            $response = array(
                "success" => true,
                "total" => (int) $total,
                "page" => (int) $page,
                "per_page" => (int) $per_page,
                "data" => $products
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function updateBulk(Request $request, Config $config)
    {
        DB::beginTransaction();

        try {
            $data = $request->only('meta_data');

            $validator = Validator::make($data, [
                'meta_data' => "required|array",
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            foreach ($data['meta_data'] as $item) {
                $dataInput = $item;

                if (is_object($item['meta_value'])) {
                    $imageName = $item['meta_value']->getClientOriginalName();

                    $item['meta_value']->storeAs('images/config', $imageName);
                    $dataInput = array_merge($item, ['meta_value' => $imageName]);
                }

                DB::table('configs')->where('meta_key', $item['meta_key'])->updateOrInsert(['meta_key' => $item['meta_key']], $dataInput);
            }
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update config']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('meta_key', 'meta_value');

            $validator = Validator::make($data, [
                'meta_key' => "required",
                'meta_value' => "required",
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }


            DB::table('configs')->upsert($data, ['meta_key']);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update config']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function updateBanner(Request $request)
    {
        DB::beginTransaction();
        try {
            if ($request->has('banner') && is_array($request->get('banner'))) {
                $data = $request->all();
                $dataBanner = $data['banner'];

                DB::table('configs')->where('meta_key', 'ilike', 'banner_%')->delete();

                $dataInput = array();

                foreach ($dataBanner as $value) {
                    if (gettype($value['meta_value']) === 'string') {
                        $imageName = $value['meta_value'];
                    } else {
                        $today = time();
                        $imageName = str_replace(' ', '-', $value['meta_key']) . $today . '.' . $value['meta_value']->extension();

                        $value['meta_value']->storeAs('images/banner', $imageName);
                    }

                    $dataInput[] = array('meta_key' => $value['meta_key'], 'meta_value' => $imageName);
                }

                DB::table('configs')->insert($dataInput);
                // all good
                DB::commit();
                return response()->json(['success' => true, 'message' => 'Success update banner']);
            }
            return response()->json(['success' => true, 'message' => 'Please upload at least one image'], 422);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}
