<?php

namespace App\Http\Controllers;

use App\Models\BannerSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BannerAdminController extends Controller
{
    public function list(Request $request)
    {
        try {
            // // Pagination
            $page = $request->get('page', 1);
            $per_page = $request->get('per_page', 10);
            $offset = $per_page * ($page - 1);

            // Sorting
            $sort = $request->get('sort', 'order');
            $order = $request->get('order', 'asc');

            $total = BannerSetting::select('count(id)')->count();

            $products = BannerSetting::orderBy('active', 'DESC')->orderBy($sort, $order)->skip($offset)
                ->take($per_page)
                ->get();

            $response = array(
                "success" => true,
                "total" => (int) $total,
                "page" => (int) $page,
                "per_page" => (int) $per_page,
                "data" => $products
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();

        try {
            $data = $request->only('photo_url', 'order', 'target_url');

            $validator = Validator::make($data, [
                'order' => 'required|numeric|unique:banner_settings',
                'photo_url' => 'required|image|mimes:jpg,jpeg,png|max:1024',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $imageName = $request->photo_url->getClientOriginalName();
            $request->photo_url->storeAs('images/banner', $imageName);
            $dataInput = array_merge($data, ['photo_url' => $imageName]);

            DB::table('banner_settings')->insert($dataInput);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success create variant'], 201);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request, BannerSetting $banner)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('photo_url', 'order', 'target_url');
            $id = $banner->id;

            $validator = Validator::make($data, [
                'order' => ["numeric", Rule::unique('banner_settings')->ignore($id)],
                'photo_url' => 'image|mimes:jpg,jpeg,png|max:1024',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $dataInput = $data;
            if ($request->has('photo_url')) {
                $imageName = $request->photo_url->getClientOriginalName();
                $request->photo_url->storeAs('images/banner', $imageName);
                $dataInput = array_merge($data, ['photo_url' => $imageName]);
            }

            DB::table('banner_settings')->where('id', $id)->update($dataInput);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update banner']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function updateActive(Request $request, BannerSetting $banner)
    {
        
        DB::beginTransaction();
        try {
            $data = $request->only('active');
            $id = $banner->id;

            $validator = Validator::make($data, [
                'active' => "numeric",
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            DB::table('banner_settings')->where('id', $id)->update($data);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update banner']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function delete(Request $request, BannerSetting $banner)
    {
        DB::beginTransaction();

        try {
            if (isset($banner->photo_url) && file_exists("images/banner/" . $banner->photo_url)) {
                Storage::delete("images/banner/" . $banner->photo_url);
            }
            DB::table('banner_settings')->delete($banner->id);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success delete banner']);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}
