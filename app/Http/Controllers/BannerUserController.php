<?php

namespace App\Http\Controllers;

use App\Models\BannerSetting;
use Illuminate\Http\Request;

class BannerUserController extends Controller
{
    public function list(Request $request)
    {
        try {

            $banner = BannerSetting::where('active', 1)
                ->get();

            $response = array(
                "success" => true,
                "data" => $banner
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }
}
