<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PageAdminController extends Controller
{
    public function list(Request $request)
    {
        try {
            // // Pagination
            $page = $request->get('page', 1);
            $per_page = $request->get('per_page', 10);
            $offset = $per_page * ($page - 1);

            // Search
            $search = $request->get('search', '');

            // Sorting
            $sort = $request->get('sort', 'order');
            $order = $request->get('order', 'asc');

            $total = Page::select('count(id)')->where(function ($query) use ($search) {
                $query->where('title', 'ilike', '%' . $search . '%')
                    ->orWhere('content', 'ilike', '%' . $search . '%');
            })->count();

            $products = Page::where(function ($query) use ($search) {
                $query->where('title', 'ilike', '%' . $search . '%')
                    ->orWhere('content', 'ilike', '%' . $search . '%');
            })->orderBy($sort, $order)->skip($offset)
                ->take($per_page)
                ->get();

            $response = array(
                "success" => true,
                "total" => (int) $total,
                "page" => (int) $page,
                "per_page" => (int) $per_page,
                "data" => $products
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function getById(Request $request, Page $page)
    {
        try {
            $selected_page = Page::findOrFail($page['id']);
            return response()->json(['success' => true, 'data' => $selected_page]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();

        try {
            $data = $request->only('photo_url', 'title', 'content', 'slug');

            $validator = Validator::make($data, [
                'title' => 'required|unique:pages',
                'slug' => 'required|unique:pages',
                'content' => 'required',
                'photo_url' => 'image|mimes:jpg,jpeg,png|max:1024',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }
            $imageName = null;
            if ($request->hasFile('photo_url')) {
                $imageName = $request->photo_url->getClientOriginalName();
                $request->photo_url->storeAs('images/page', $imageName);
            }
            $dataInput = array_merge($data, ['photo_url' => $imageName]);

            DB::table('pages')->insert($dataInput);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success create page'], 201);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request, Page $page)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('photo_url', 'title', 'content', 'slug');
            $id = $page->id;

            $validator = Validator::make($data, [
                'title' => ['required', Rule::unique('pages')->ignore($id)],
                'slug' => ['required', Rule::unique('pages')->ignore($id)],
                'content' => 'required',
                'photo_url' => 'image|mimes:jpg,jpeg,png|max:1024',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $dataInput = $data;
            if ($request->has('photo_url')) {
                $imageName = $request->photo_url->getClientOriginalName();
                $request->photo_url->storeAs('images/page', $imageName);
                $dataInput = array_merge($data, ['photo_url' => $imageName]);
            }

            DB::table('pages')->where('id', $id)->update($dataInput);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update page']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function delete(Request $request, Page $page)
    {
        DB::beginTransaction();

        try {

            if (isset($page->photo_url) && file_exists("images/page/" . $page->photo_url)) {
                Storage::delete("images/page/" . $page->photo_url);
            }

            DB::table('pages')->where('id', $page->id)->delete();

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success delete page']);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}
