<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\VariantItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProductAdminController extends Controller
{
    public function list(Request $request)
    {
        try {
            // // Pagination
            $page = $request->get('page', 1);
            $per_page = $request->get('per_page', 10);
            $offset = $per_page * ($page - 1);

            // Search
            $search = $request->get('search', '');

            // Sorting
            $sort = $request->get('sort', 'updated_at');
            $order = $request->get('order', 'desc');

            $total = Product::select('count(id)')->where('name', 'ilike', '%' . $search . '%')->count();

            $products = Product::where('name', 'ilike', '%' . $search . '%')->orderBy($sort, $order)->skip($offset)
                ->take($per_page)
                ->get();

            $response = array(
                "success" => true,
                "total" => (int) $total,
                "page" => (int) $page,
                "per_page" => (int) $per_page,
                "data" => $products
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function getById(Request $request, Product $product)
    {
        try {
            $selected_product = Product::findOrFail($product['id']);
            return response()->json(['success' => true, 'data' => $selected_product]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function getVariantItem(Request $request, Product $product)
    {
        try {
            $variant_items = VariantItem::with(['product_variant'])->where('product_id', '=', $product['id'])->get();
            return response()->json(['success' => true, 'data' => $variant_items]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();

        try {
            $data = $request->only('photo_url', 'name', 'price', 'description');

            $validator = Validator::make($data, [
                'photo_url' => 'required|image|mimes:jpeg,png,jpg|max:1024',
                'name' => 'required|unique:products|max:255',
                'price' => 'required|numeric'
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $today = time();

            $imageName = str_replace(' ', '-', $request->input('name')) . $today . '.' . $request->photo_url->extension();

            $request->photo_url->storeAs('images/products', $imageName);

            $dataInput = array_merge($data, ['photo_url' => $imageName], ['price' => floatval($request->input('price'))]);


            DB::table('products')->insert($dataInput);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success create product'], 201);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request, Product $product)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('photo_url', 'name', 'price', 'description');
            $id = $product->id;

            $validator = Validator::make($data, [
                'photo_url' => 'image|mimes:jpeg,png,jpg|max:1024',
                'name' => ["max:255", Rule::unique('products')->ignore($id)],
                'price' => 'numeric'
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            if (null !== $request->photo_url) {
                $today = time();
                $imageName = str_replace(' ', '-', $request->input('name')) . $today . '.' . $request->photo_url->extension();

                $request->photo_url->storeAs('images/products', $imageName);
                $data = array_merge($data, ['photo_url' => $imageName]);
            }


            DB::table('products')->where('id', $id)->update($data);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update product']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function delete(Request $request, Product $product)
    {
        DB::beginTransaction();

        try {
            if (isset($product->photo_url) && file_exists("images/products/" . $product->photo_url)) {
                Storage::delete("images/products/" . $product->photo_url);
            }
            DB::table('products')->delete($product->id);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success delete product']);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function updateVariantItem(Request $request, $id)
    {
        DB::beginTransaction();

        try {

            $data = $request->all();

            $newItem = isset($data['new']) ? $data['new'] : [];
            $deleteItem = isset($data['delete']) ? $data['delete'] : [];
            $editItem = isset($data['edit']) ? $data['edit'] : [];

            if (count($editItem) > 0) {
                foreach ($editItem as $item) {

                    if (isset($item['photo_url'])) {

                        if (gettype($item['photo_url']) === 'string') {
                            if ($item['photo_url'] == '' || $item['photo_url'] == 'null' || $item['photo_url'] == null) {
                                $imageName = null;
                                $dataEdit = array_merge($item, ['photo_url' => $imageName]);
                            } else {
                                $dataEdit = $item;
                            }
                        } else {
                            $today = time();
                            $imageName = str_replace(' ', '-', $item['name']) . $today . '.' . $item['photo_url']->extension();

                            $item['photo_url']->storeAs('images/products', $imageName);
                            $dataEdit = array_merge($item, ['photo_url' => $imageName]);
                        }
                    }

                    if (!isset($item['extra_price'])) {
                        $dataEdit = array_merge($item, ['extra_price' => 0]);
                    }

                    DB::table('variant_items')->where('id', $item['id'])->where('product_id', $id)->update($dataEdit);
                }
            }

            if (count($newItem) > 0) {

                foreach ($newItem as $item) {
                    $dataNew = $item;
                    if (isset($item['photo_url'])) {
                        $today = time();
                        $imageName = str_replace(' ', '-', $item['name']) . $today . '.' . $item['photo_url']->extension();

                        $item['photo_url']->storeAs('images/products', $imageName);
                        // Storage::disk('local')->put('images/products', $item['photo_url'], $imageName);

                        $dataNew = array_merge($item, ['photo_url' => $imageName]);
                    }
                    if (!isset($item['extra_price'])) {
                        $dataNew = array_merge($item, ['extra_price' => 0]);
                    }
                    DB::table('variant_items')->insert($dataNew);
                }
            }

            if (count($deleteItem) > 0) {
                DB::table('variant_items')->where('product_id', $id)->whereIn('id', $deleteItem)->delete();
            }

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update variant items']);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}
