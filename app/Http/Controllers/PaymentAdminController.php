<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PaymentAdminController extends Controller
{
    public function list(Request $request)
    {
        try {
            // // Pagination
            $page = $request->get('page', 1);
            $per_page = $request->get('per_page', 10);
            $offset = $per_page * ($page - 1);

            // Search
            $search = $request->get('search', '');

            // Sorting
            $sort = $request->get('sort', 'updated_at');
            $order = $request->get('order', 'desc');

            $total = Payment::select('count(id)')->where(function ($query) use ($search) {
                $query->where('bank_name', 'ilike', '%' . $search . '%')
                    ->orWhere('bank_no', 'ilike', '%' . $search . '%')
                    ->orWhere('atas_nama', 'ilike', '%' . $search . '%');
            })->count();

            $payments = Payment::where(function ($query) use ($search) {
                $query->where('bank_name', 'ilike', '%' . $search . '%')
                    ->orWhere('bank_no', 'ilike', '%' . $search . '%');
            })
                ->orderBy('active', 'DESC')
                ->orderBy($sort, $order)->skip($offset)
                ->take($per_page)
                ->get();

            $response = array(
                "success" => true,
                "total" => (int) $total,
                "page" => (int) $page,
                "per_page" => (int) $per_page,
                "data" => $payments
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();

        try {
            $data = $request->only('photo_url', 'bank_name', 'bank_no', 'atas_nama');

            $validator = Validator::make($data, [
                'bank_name' => 'required',
                'bank_no' => 'required',
                'atas_nama' => 'required',
                'photo_url' => 'required|image|mimes:jpg,jpeg,png|max:1024',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $imageName = $request->photo_url->getClientOriginalName();
            $request->photo_url->storeAs('images/payment', $imageName);
            $dataInput = array_merge($data, ['photo_url' => $imageName]);

            DB::table('payments')->insert($dataInput);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success create payment methods'], 201);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request, Payment $payment)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('photo_url', 'bank_name', 'bank_no', 'atas_nama');
            $id = $payment->id;

            $validator = Validator::make($data, [
                'bank_name' => 'required',
                'bank_no' => 'required',
                'atas_nama' => 'required',
                'photo_url' => 'image|mimes:jpg,jpeg,png|max:1024',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $dataInput = $data;

            if ($request->has('photo_url')) {
                $imageName = $request->photo_url->getClientOriginalName();
                $request->photo_url->storeAs('images/payment', $imageName);
                $dataInput = array_merge($data, ['photo_url' => $imageName]);
            }

            DB::table('payments')->where('id', $id)->update($dataInput);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update payment method']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
    public function updateActive(Request $request, Payment $payment)
    {

        DB::beginTransaction();
        try {
            $data = $request->only('active');
            $id = $payment->id;
            print_r($request->only('active'));

            $validator = Validator::make($data, [
                'active' => "numeric",
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            DB::table('payments')->where('id', $id)->update($data);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update payment methods']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function delete(Request $request, Payment $payment)
    {
        DB::beginTransaction();

        try {

            DB::table('payments')->delete($payment->id);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success delete payment methods']);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}
