<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentUserController extends Controller
{
    public function list(Request $request)
    {
        try {

            $methods = Payment::where('active', 1)
                ->get();

            $response = array(
                "success" => true,
                "data" => $methods
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }
}
