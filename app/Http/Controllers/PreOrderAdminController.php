<?php

namespace App\Http\Controllers;

use App\Models\PreOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class PreOrderAdminController extends Controller
{
    public function list(Request $request)
    {
        try {
            // // Pagination
            $page = $request->get('page', 1);
            $per_page = $request->get('per_page', 10);
            $offset = $per_page * ($page - 1);

            // Search
            $search = $request->get('search', '');
            $product = $request->get('product', '');

            // Sorting
            $sort = $request->get('sort', 'updated_at');
            $order = $request->get('order', 'desc');

            if (($request->has('start_date') && $request->get('start_date')) && ($request->has('end_date') && $request->get('end_date'))) {
                $start_date = $request->get('start_date');
                $end_date = $request->get('end_date');
                $total = PreOrder::select('count(id)')
                    ->where('title', 'ilike', '%' . $search . '%')
                    ->whereHas('product', function ($q) use ($product) {
                        $q->where('name', 'ilike', '%' . $product . '%');
                    })
                    ->where('start_date', '>=', $start_date)
                    ->where('end_date', '<=', $end_date)->count();

                $preOrder = PreOrder::with('product')
                    ->where('title', 'ilike', '%' . $search . '%')
                    ->whereHas('product', function ($q) use ($product) {
                        $q->where('name', 'ilike', '%' . $product . '%');
                    })
                    ->where('start_date', '>=', $start_date)
                    ->where('end_date', '<=', $end_date)
                    ->orderBy('active', 'desc')
                    ->orderBy($sort, $order)->skip($offset)
                    ->take($per_page)
                    ->get();
            } else {
                $total = PreOrder::select('count(id)')
                    ->where('title', 'ilike', '%' . $search . '%')
                    ->whereHas('product', function ($q) use ($product) {
                        $q->where('name', 'ilike', '%' . $product . '%');
                    })->count();

                $preOrder = PreOrder::with('product')
                    ->where('title', 'ilike', '%' . $search . '%')
                    ->whereHas('product', function ($q) use ($product) {
                        $q->where('name', 'ilike', '%' . $product . '%');
                    })
                    ->orderBy('active', 'desc')
                    ->orderBy($sort, $order)->skip($offset)
                    ->take($per_page)
                    ->get();
            }

            $response = array(
                "success" => true,
                "total" => (int) $total,
                "page" => (int) $page,
                "per_page" => (int) $per_page,
                "data" => $preOrder
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function getById(Request $request, PreOrder $preorder)
    {
        try {
            $selected_po = PreOrder::with(['product', 'pre_order_metas'])->findOrFail($preorder['id']);
            return response()->json(['success' => true, 'data' => $selected_po]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();

        try {
            $data = $request->only('photo_url', 'title', 'product_id', 'description', 'start_date', 'end_date', 'slug');

            $validator = Validator::make($data, [
                'photo_url' => 'required',
                'title' => 'required|unique:pre_orders|max:255',
                'slug' => 'required|unique:pre_orders|max:255',
                'start_date' => 'required|date',
                'end_date' => 'required|date',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }



            if (gettype($data['photo_url']) === 'string') {
                $imageName = $data['photo_url'];
            } else {
                $today = time();
                $imageName = str_replace(' ', '-', $request->input('title')) . $today . '.' . $request->photo_url->extension();

                $request->photo_url->storeAs('images/products', $imageName);
            }

            $dataInput = array_merge($data, ['photo_url' => $imageName]);


            $poId = DB::table('pre_orders')->insertGetId($dataInput);

            if ($request->has('meta') && is_array($request->get('meta'))) {
                $data_meta = array();
                foreach ($request->get('meta') as $key => $value) {
                    foreach ($value as $meta_value) {
                        $data_meta[] = array('pre_order_id' => $poId, 'meta_key' => $key, 'meta_value' => $meta_value);
                    }
                }

                DB::table('pre_order_metas')->insert($data_meta);
            }

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success create pre order'], 201);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request, PreOrder $preorder)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('title', 'slug', 'description', 'start_date', 'end_date', 'photo_url', 'photo_url');
            $id = $preorder->id;

            $validator = Validator::make($data, [
                'title' => ["max:255", Rule::unique('pre_orders')->ignore($id)],
                'slug' => ["max:255", Rule::unique('pre_orders')->ignore($id)],
                'start_date' => 'required|date',
                'end_date' => 'required|date',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            if ($request->has('photo_url')) {
                if (gettype($data['photo_url']) === 'string') {
                    $imageName = $data['photo_url'];
                } else {
                    $today = time();
                    $imageName = str_replace(' ', '-', $data['title']) . $today . '.' . $data['photo_url']->extension();

                    $data['photo_url']->storeAs('images/products', $imageName);
                    $dataEdit = array_merge($data, ['photo_url' => $imageName]);
                }
                $dataEdit = array_merge($data, ['photo_url' => $imageName]);
            }

            if ($request->has('meta') && is_array($request->get('meta'))) {
                // IF EDIT META, RE CREATE META DATA
                DB::table('pre_order_metas')->where('pre_order_id', '=', $id)->delete();
                $data_meta = array();
                foreach ($request->get('meta') as $key => $value) {
                    foreach ($value as $meta_value) {
                        $data_meta[] = array('pre_order_id' => $id, 'meta_key' => $key, 'meta_value' => $meta_value);
                    }
                }

                DB::table('pre_order_metas')->insert($data_meta);
            }


            DB::table('pre_orders')->where('id', $id)->update($dataEdit);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update pre order']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function updateActive(Request $request, PreOrder $preorder)
    {

        DB::beginTransaction();
        try {
            $data = $request->only('active');
            $id = $preorder->id;

            $validator = Validator::make($data, [
                'active' => "numeric",
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            DB::table('pre_orders')->where('id', $id)->update($data);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update pre order']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }


    public function delete(Request $request, PreOrder $preorder)
    {
        DB::beginTransaction();

        try {

            if (isset($preorder->photo_url) && file_exists("images/products/" . $preorder->photo_url)) {
                Storage::delete("images/products/" . $preorder->photo_url);
            }

            DB::table('pre_orders')->delete($preorder->id);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success delete pre order']);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}
