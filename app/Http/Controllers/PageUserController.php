<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageUserController extends Controller
{
    public function getDetail(Request $request)
    {
        try {
            $slug = $request->get('slug', '');
            if (!isset($slug)) {
                return response()->json(['success' => true, 'message' => "Page not found"], 404);
            }
            $selected_page = Page::where('slug', $slug)->get();

            if (isset($selected_page) && count($selected_page)) {
                return response()->json(['success' => true, 'data' => $selected_page[0]]);
            }

            return response()->json(['success' => true, 'message' => "Page not found"], 404);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }
}
