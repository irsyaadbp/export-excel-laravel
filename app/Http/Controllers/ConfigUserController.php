<?php

namespace App\Http\Controllers;

use App\Models\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ConfigUserController extends Controller
{
    public function list(Request $request)
    {
        try {
            // // Pagination
            $page = $request->get('page', 1);
            $per_page = $request->get('per_page', 10);
            $offset = $per_page * ($page - 1);

            // Search
            $search = $request->get('search', '');

            // Sorting
            $sort = $request->get('sort', 'updated_at');
            $order = $request->get('order', 'desc');

            $total = Config::select('count(id)')->where('meta_key', 'ilike', $search . '%')->count();

            $products = Config::where('meta_key', 'ilike', $search . '%')->orderBy($sort, $order)->skip($offset)
                ->take($per_page)
                ->get();

            $response = array(
                "success" => true,
                "total" => (int) $total,
                "page" => (int) $page,
                "per_page" => (int) $per_page,
                "data" => $products
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function updateBulk(Request $request, Config $config)
    {
        DB::beginTransaction();

        try {
            $data = $request->only('meta_data');

            $validator = Validator::make($data, [
                'meta_data' => "required|array",
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            foreach ($data['meta_data'] as $item) {
                $dataInput = $item;

                if (is_object($item['meta_value'])) {
                    $imageName = $item['meta_value']->getClientOriginalName();

                    $item['meta_value']->storeAs('images/config', $imageName);
                    $dataInput = array_merge($item, ['meta_value' => $imageName]);
                }

                DB::table('configs')->where('meta_key', $item['meta_key'])->updateOrInsert(['meta_key' => $item['meta_key']], $dataInput);
            }
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update config']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}
