<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Config;
use App\Models\VariantItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CartUserController extends Controller
{
    public function list(Request $request)
    {
        try {
            $user = Auth::user();

            $allCarts = Cart::with(['pre_order' => function ($product) {
                $product->with(['product', 'pre_order_metas' => function ($pre_order_meta) {
                    $pre_order_meta->where('meta_key', 'holiday_date')->get();
                }])->get();
            }, 'cart_metas'])->where('user_id', $user->id)->get();
            $today = Carbon::now()->toDateString();

            $carts = [];
            $carts_expired = [];

            foreach ($allCarts as $cart) {
                $end_date = Carbon::parse($cart->pre_order->end_date);
                if ($end_date->diffInDays($today) >= 2 && $cart->pre_order->active == 1) {
                    $carts[] = $cart;
                } else {
                    $carts_expired[] = $cart->id;
                }
            }

            if (isset($carts_expired)) {
                Cart::destroy($carts_expired);
            }

            $response = array(
                "success" => true,
                "data" => $carts,
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function listCheckout(Request $request)
    {
        try {
            $user = Auth::user();

            $allCarts = Cart::with(['pre_order' => function ($product) {
                $product->with(['product', 'pre_order_metas' => function ($pre_order_meta) {
                    $pre_order_meta->where('meta_key', 'delivery_type')->get();
                }])->get();
            }, 'cart_metas'])->where('user_id', $user->id)->get();
            $today = Carbon::now()->toDateString();

            $carts = [];
            $carts_expired = [];

            foreach ($allCarts as $cart) {
                $end_date = Carbon::parse($cart->pre_order->end_date);
                if ($end_date->diffInDays($today) >= 2 && $cart->pre_order->active == 1) {
                    $carts[] = $cart;
                } else {
                    $carts_expired[] = $cart->id;
                }
            }

            if (isset($carts_expired)) {
                Cart::destroy($carts_expired);
            }

            $response = array(
                "success" => true,
                "data" => $carts,
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function listConfigCheckout(Request $request)
    {
        try {
            $config_keys = ['config_delivery_fee_kilometer', 'config_delivery_max_distance', 'config_latitude_shop', 'config_longitude_shop', 'config_pickup_max_time_start', 'config_pickup_max_time_end', 'config_delivery_max_time_start', 'config_delivery_max_time_end'];
            $configs = Config::whereIn('meta_key', $config_keys)->get();

            $response = array(
                "success" => true,
                "data" => $configs,
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();

        try {

            $data = $request->only(
                'pre_order_id',
                'qty',
                'note',
                'variants',
                'lettering_on_cake',
                'lettering_on_cake_board',
                'greeting_card',
                'describe',
            );

            $validator = Validator::make($data, [
                'pre_order_id' => 'required',
                'qty' => 'numeric',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $user = Auth::user();

            $cart_id = DB::table('carts')->insertGetId(['pre_order_id' => $data['pre_order_id'], 'user_id' => $user->id]);

            $default_meta_data = ['meta_key' => '', 'meta_value' => '', 'cart_id' => $cart_id];

            $meta_data = [];

            if ($request->has('photos')) {
                foreach ($request->file('photos') as $photo) {
                    $oriName = str_replace('.' . $photo->extension(), '', $photo->getClientOriginalName());
                    $today = time();
                    $imageName = str_replace(' ', '-', $oriName) . $today . '.' . $photo->extension();
                    $photo->storeAs('images/carts', $imageName);

                    $meta_photo = array_merge($default_meta_data, ['meta_key' => 'custom_photo', 'meta_value' => $imageName]);

                    array_push($meta_data, $meta_photo);
                }
            }

            if ($request->has('variants') && is_array($data['variants'])) {
                foreach ($data['variants'] as $variant_item) {
                    $meta_variants = array_merge($default_meta_data, ['meta_key' => 'variant_item', 'meta_value' => $variant_item]);

                    array_push($meta_data, $meta_variants);
                }
            }

            array_push($meta_data, array_merge($default_meta_data, ['meta_key' => 'qty', 'meta_value' => $data['qty']]));

            if (isset($data['lettering_on_cake'])) {
                array_push($meta_data, array_merge($default_meta_data, ['meta_key' => 'lettering_on_cake', 'meta_value' => $data['lettering_on_cake']]));
            }

            if (isset($data['lettering_on_cake_board'])) {
                array_push($meta_data, array_merge($default_meta_data, ['meta_key' => 'lettering_on_cake_board', 'meta_value' => $data['lettering_on_cake_board']]));
            }
            if (isset($data['greeting_card'])) {
                array_push($meta_data, array_merge($default_meta_data, ['meta_key' => 'greeting_card', 'meta_value' => $data['greeting_card']]));
            }
            if (isset($data['describe'])) {
                array_push($meta_data, array_merge($default_meta_data, ['meta_key' => 'describe', 'meta_value' => $data['describe']]));
            }
            if (isset($data['note'])) {
                array_push($meta_data, array_merge($default_meta_data, ['meta_key' => 'note', 'meta_value' => $data['note']]));
            }


            DB::table('cart_metas')->insert($meta_data);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success insert to cart',], 201);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function getCartVariantItem(Request $request)
    {
        $ids = $request->get('variant_item_id', []);

        $variant_items = VariantItem::with(['product_variant'])->find($ids);
        $response = array(
            "success" => true,
            "data" => $variant_items,
        );

        return response()->json($response);
    }

    public function delete(Request $request, Cart $cart)
    {
        DB::beginTransaction();
        try {
            if (isset($cart)) {
                $user = Auth::user();
                if ($cart->user_id === $user->id) {
                    $custom_photo = DB::table('cart_metas')->where('meta_key', 'custom_photo')->where('cart_id', $cart->id)->get();
                    if (isset($custom_photo)) {
                        foreach ($custom_photo as $photo) {
                            if (isset($photo->meta_value) && file_exists("images/carts/" . $photo->meta_value)) {
                                Storage::delete("images/carts/" . $photo->meta_value);
                            }
                        }
                    }

                    DB::table('carts')->delete($cart->id);

                    // all good
                    DB::commit();
                    return response()->json(['success' => true, 'message' => 'Success delete cart']);
                }

                return response()->json(['success' => false, 'message' => 'User not valid'], 400);
            }
            return response()->json(['success' => false, 'message' => 'Cart item not found'], 404);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}
