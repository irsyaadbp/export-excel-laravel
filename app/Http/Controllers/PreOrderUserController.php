<?php

namespace App\Http\Controllers;

use App\Models\PreOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PreOrderUserController extends Controller
{
    public function list(Request $request)
    {
        try {
            // Search
            $search = $request->get('search', '');

            // Sorting
            $sort = $request->get('sort', 'updated_at');
            $order = $request->get('order', 'desc');
            $end_date = Carbon::now();

            // TODO: SEARCH BY PRODUCT NAME & FILTER END DATE
            $preOrder = PreOrder::with('product')
                ->where('title', 'ilike', '%' . $search . '%')
                ->where('active', '1')
                ->where('end_date', '>=', $end_date->toDateString())
                ->orderBy($sort, $order)
                ->get();

            $response = array(
                "success" => true,
                "end_date" => $end_date->toDateString(),
                "data" => $preOrder
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function getBySlug(Request $request, PreOrder $preorder)
    {
        try {
            $selected_preorder = PreOrder::with(['product' => function ($product) {
                $product->with(['variant_items' => function ($product) {
                    $product->with(['product_variant'])->get();
                }])->get();
            }, 'pre_order_metas'])->where('slug', $preorder['slug'])->get();
            if (isset($selected_preorder)) {

                return response()->json(['success' => true, 'data' => $selected_preorder, 'slug' => $preorder['slug']]);
            }
            return response()->json(['success' => false, "message" => "Pre Order not found"], 404);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }
}
