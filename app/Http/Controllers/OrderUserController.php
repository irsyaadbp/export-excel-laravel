<?php

namespace App\Http\Controllers;

use App\Mail\PaymentUploadMail;
use App\Models\Order;
use App\Models\VariantItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class OrderUserController extends Controller
{
    public function list(Request $request)
    {
        try {
            // Search
            $search = $request->get('search', '');
            $status = $request->get('status', '');

            // Sorting
            $sort = $request->get('sort', 'status');
            $order = $request->get('order', 'desc');
            $user = Auth::user();

            $orders = Order::where('order_number', 'ilike', '%' . $search . '%')
                ->where('user_id', $user->id)
                ->when($status, function ($q) use ($status) {
                    return $q->where('status', $status);
                })
                ->orderBy('order_number', 'desc')
                ->orderBy($sort, $order)
                ->get();

            $response = array(
                "success" => true,
                "data" => $orders
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function getDetail(Request $request)
    {
        try {
            $orderNumber = $request->get('orderNumber', '');

            if (!$orderNumber) {
                return response()->json(['success' => false, "message" => "Order Number can't be empty"], 422);
            }
            // order meta
            // order history

            // order item
            // order item meta
            // order item variants

            $selected_order = Order::with(['order_item' => function ($product) {
                $product->with(['order_item_meta', 'order_item_variants'])->get();
            }, 'order_meta', 'history' => function ($history) {
                $history->with(['user_detail'])->orderBy('id', 'asc')->get();
            }, 'payment_method'])->where('order_number', $orderNumber)->first();

            $user = Auth::user();

            if (isset($selected_order) && $selected_order->user_id === $user->id) {
                return response()->json(['success' => true, 'data' => $selected_order]);
            }
            return response()->json(['success' => true, 'message' => "Order number not found"], 404);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();
        try {
            // {
            //     "order_date": "22 October 2021 10:00:00",
            //     "delivery_type": "delivery",
            //     "address": {
            //         "name": "Irsyaad Budi",
            //         "label": "Rumah",
            //         "phone": "089723424234",
            //         "address": "Jalan Wates",
            //         "note": "rumah warna ungu"
            //     },
            //     "payment_methods_id": 1,
            //     "price": {
            //         "grand_total": 90000,
            //         "total_product": 80000,
            //         "delivery": 10000
            //     },
            //     "detail": [
            //         {
            //             "title": "October Slot Spons Cake",
            //             "product_name": "Spons Cake",
            //             "price_product": "65000.00",
            //             "qty": "1",
            //             "note": "kasih gambar ayah juga",
            //             "lettering_on_cake": '',
            //             "lettering_on_cake_board": '',
            //             "greeting_card": '',
            //             "describe": '',
            //             "custom_photo": [
            //                 "sponse-cake1634664347.jpg",
            //                 "Screenshot at Aug 04 15-33-251634664347.png"
            //             ],
            //             "variant_item": [
            //                 "2",
            //                 "3"
            //             ]
            //         }
            //     ]
            // }

            $data = $request->only(
                'order_date',
                'delivery_type',
                'address',
                'payment_methods_id',
                'price',
                'detail'
            );

            $validator = Validator::make($data, [
                'order_date' => 'required|date',
                'delivery_type' => 'required|in:ojek,delivery,pickup',
                'address.name' => 'string|max:255',
                'address.label' => 'string|max:255',
                'address.address' => 'string',
                'address.phone' => 'string|max:255',
                'payment_methods_id' => 'required',
                'price.grand_total' => 'required|numeric',
                'price.total_product' => 'required|numeric',
                'price.delivery' => 'required|numeric',
                'detail.*.title' => 'required|string|max:255',
                'detail.*.product_name' => 'required|string|max:255',
                'detail.*.price_product' => 'required|numeric',
                'detail.*.qty' => 'required|numeric',
                'detail.*.lettering_on_cake' => 'string',
                'detail.*.lettering_on_cake_board' => 'string',
                'detail.*.greeting_card' => 'string',
                'detail.*.describe' => 'string',
                'detail.*.note' => 'string',
                'detail.*.custom_photo' => 'min:0',
                'detail.*.custom_photo.*' => 'required|string',
                'detail.*.variant_item.*' => 'min:0',
            ]);


            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $user = Auth::user();
            /**
             * 1. Create order table
             *  $table->foreignId('user_id');
             *  $table->string('order_number');
             *  $table->string('order_date');
             *  $table->string('delivery_type');
             *  $table->decimal('total_price', 24, 2);
             *  $table->string('status', 20);
             */

            $order_number = $this->generateOrderNR();
            $order_id = DB::table('orders')->insertGetId([
                'user_id' => $user->id,
                'order_number' => $order_number,
                'order_date' => $data['order_date'],
                'delivery_type' => $data['delivery_type'],
                'payment_id' => $data['payment_methods_id'],
                'total_price' => $data['price']['grand_total'],
                'status' => 'waiting_payment',
            ]);

            /**
             * 2. Create Order Meta (ex: address, payment methods)
             *  $table->foreignId('order_id')->constrained()->onDelete('cascade');
             *  $table->string('meta_key');
             *  $table->string('meta_value');
             */
            $meta_data = [];

            foreach ($data['address'] as $key => $value) {
                array_push($meta_data, ['order_id' => $order_id, 'meta_key' => "address_{$key}", 'meta_value' => $value]);
            }
            array_push($meta_data, ['order_id' => $order_id, 'meta_key' => "total_price_product", 'meta_value' => $data['price']['total_product']]);
            array_push($meta_data, ['order_id' => $order_id, 'meta_key' => "delivery_price", 'meta_value' => $data['price']['delivery']]);

            DB::table('order_metas')->insert($meta_data);
            $metas = [];
            foreach ($data['detail'] as $value) {
                $variants = VariantItem::with('product_variant')->whereIn('id', $value['variant_item'])->get();
                $total_price = (float) $value['price_product'] * (int) $value['qty'];

                $variant_items = [];
                foreach ($variants as $item) {
                    $total_price += ((float) $item->extra_price * ((int) $value['qty']));
                }

                /**
                 * 3. Create Order Item
                 *  $table->foreignId('order_id')->constrained()->onDelete('cascade');
                 *  $table->string('po_title');
                 *  $table->string('product_name');
                 *  $table->decimal('product_price', 24, 2);
                 *  $table->decimal('total_price', 24, 2);
                 *  $table->string('qty');
                 */

                $order_item_id = DB::table('order_items')->insertGetId([
                    'order_id' => $order_id,
                    'po_title' => $value['title'],
                    'product_name' => $value['product_name'],
                    'product_price' => $value['price_product'],
                    'total_price' => $total_price,
                    'qty' => (int) $value['qty'],
                ]);

                /**
                 * 4. Create Order Item Meta (ex: custom photo, note)
                 *  $table->foreignId('order_item_id')->constrained()->onDelete('cascade');
                 *  $table->string('meta_key');
                 *  $table->string('meta_value');
                 */
                $metas = [];

                if (isset($value['lettering_on_cake'])) {
                    $metas[] = ['order_item_id' => $order_item_id, 'meta_key' => 'lettering_on_cake', 'meta_value' => $value['lettering_on_cake']];
                }
                if (isset($value['lettering_on_cake_board'])) {
                    $metas[] = ['order_item_id' => $order_item_id, 'meta_key' => 'lettering_on_cake_board', 'meta_value' => $value['lettering_on_cake_board']];
                }

                if (isset($value['greeting_card'])) {
                    $metas[] = ['order_item_id' => $order_item_id, 'meta_key' => 'greeting_card', 'meta_value' => $value['greeting_card']];
                }
                if (isset($value['describe'])) {
                    $metas[] = ['order_item_id' => $order_item_id, 'meta_key' => 'describe', 'meta_value' => $value['describe']];
                }

                if (isset($value['note'])) {
                    $metas[] = ['order_item_id' => $order_item_id, 'meta_key' => 'note', 'meta_value' => $value['note']];
                }
                foreach ($value['custom_photo'] as $photo_name) {
                    $metas[] = ['order_item_id' => $order_item_id, 'meta_key' => 'custom_photo', 'meta_value' => $photo_name];
                }

                DB::table('order_item_metas')->insert($metas);

                /**
                 * 5. Create Order Item Variant
                 *  $table->foreignId('order_item_id')->constrained()->onDelete('cascade');
                 *  $table->string('variant_name');
                 *  $table->string('variant_item_name');
                 *  $table->decimal('extra_price', 24, 2);
                 */

                $variant_items = [];
                foreach ($variants as $item) {
                    $variant_items[] = [
                        'order_item_id' => $order_item_id,
                        'variant_name' => $item['product_variant']['name'],
                        'variant_item_name' => $item['name'],
                        'extra_price' => $item['extra_price']
                    ];
                }

                DB::table('order_item_variants')->insert($variant_items);

                // MOVE CUSTOM PHOTO TO ORDERS
                foreach ($value['custom_photo'] as $photo_name) {
                    $path = "images/carts/{$photo_name}";
                    if (Storage::exists($path)) {
                        $order = DB::table('orders')->where('id', $order_id)->first();
                        $order_item = DB::table('order_items')->where('id', $order_item_id)->first();
                        $product_name = str_replace(' ', '-', $order_item->product_name) . '_' . $order_item_id;
                        $orderNumber = str_replace('#', '', $order->order_number);
                        $new_path = "images/orders/{$orderNumber}/{$product_name}";
                        if (!Storage::exists($new_path)) {
                            Storage::makeDirectory($new_path, 0777, true, true);
                        }
                        Storage::move($path, "{$new_path}/{$photo_name}");
                    }
                }
            }

            /**
             * 6. Create Order History
             * $table->foreignId('order_id')->constrained()->onDelete('cascade');
             * $table->string('activity');
             * $table->string('description');
             * $table->string('custom_color', 10);
             * $table->foreignId('update_by')->references('id')->on('users')
             */
            DB::table('order_histories')->insert([
                [
                    'order_id' => $order_id,
                    'activity' => "Order berhasil dibuat!",
                    'description' => "",
                    'custom_color' => "success",
                    'update_by' => $user->id
                ],
                [
                    'order_id' => $order_id,
                    'activity' => "Menunggu pembayaran",
                    'description' => "",
                    'custom_color' => "warning",
                    'update_by' => null
                ]
            ]);

            DB::table('carts')->where('user_id', $user->id)->delete();

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success create order', 'data' => ['order_number' => $order_number], 'metas' => $metas]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function updatePayment(Request $request, Order $order)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('bukti_transfer_photo', 'bukti_transfer_note',);
            $id = $order->id;


            $validator = Validator::make($data, [
                'bukti_transfer_photo' => 'required|image|mimes:jpg,jpeg,png|max:1024',
                'bukti_transfer_note' => 'max:255',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }


            if (isset($order)) {
                $user = Auth::user();
                if ($order->user_id === $user->id) {
                    $orderNumber = str_replace("#", "", $order->order_number);
                    $name = "bukti-transfer-order-{$orderNumber}";
                    $today = time();
                    $imageName = $name . $today . '.' . $request->bukti_transfer_photo->extension();
                    $request->bukti_transfer_photo->storeAs("images/orders/{$order->id}", $imageName);

                    $meta_data[] = ['order_id' => $id, 'meta_key' => "bukti_transfer_photo", 'meta_value' => $imageName];
                    $meta_data[] = ['order_id' => $id, 'meta_key' => "bukti_transfer_note", 'meta_value' => $data['bukti_transfer_note']];

                    DB::table('order_metas')->insert($meta_data);

                    DB::table('orders')->where('id', $id)->update(['status' => 'waiting_confirmation', 'updated_at' => Carbon::now()->toDateTime()]);

                    DB::table('order_histories')->insert([
                        [
                            'order_id' => $id,
                            'activity' => "Bukti transfer pembayaran telah terupload!",
                            'description' => $data['bukti_transfer_note'],
                            'custom_color' => "success",
                            'update_by' => $user->id
                        ],
                        [
                            'order_id' => $id,
                            'activity' => "Menunggu konfirmasi pembayaran dari admin",
                            'description' => '',
                            'custom_color' => "warning",
                            'update_by' => null
                        ],
                    ]);

                    $details = [
                        'user' => $user,
                        'order' => [
                            'total_price' => $this->rupiah($order->total_price),
                            'order_number' => $order->order_number,
                            'raw_order_number' => str_replace('#', "%23", $order->order_number)
                        ]
                    ];

                    $config_email = DB::table('configs')->where('meta_key', 'config_email_receiver')->first();

                    Mail::to($config_email->meta_value)->send(new PaymentUploadMail($details));

                    // all good
                    DB::commit();
                    return response()->json(['success' => true, 'message' => 'Success update payment']);
                }

                return response()->json(['success' => false, 'message' => 'User not valid'], 400);
            }

            return response()->json(['success' => false, 'message' => 'Order not found'], 404);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function updateReject(Request $request, Order $order)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('note');
            $id = $order->id;

            $validator = Validator::make($data, [
                'note' => "required|max:255",
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }
            $user = Auth::user();

            if ($order->user_id !== $user->id) {
                return response()->json(['success' => false, 'message' => 'User not valid'], 400);
            }

            DB::table('orders')->where('id', $id)->update(['status' => 'reject']);

            $bukti = DB::table('order_metas')->where('meta_key', 'bukti_transfer_photo')->where('order_id', $id)->get();

            DB::table('order_metas')->where('meta_key', 'bukti_transfer_photo')->where('order_id', $id)->delete();
            DB::table('order_metas')->where('meta_key', 'bukti_transfer_note')->where('order_id', $id)->delete();

            DB::table('order_histories')->insert([
                [
                    'order_id' => $id,
                    'activity' => "Ordermu dibatalkan!",
                    'description' => $data['note'],
                    'custom_color' => "danger",
                    'update_by' => $user->id
                ]
            ]);

            if (isset($bukti->meta_value) && file_exists("images/orders/{$id}" . $bukti->meta_value)) {
                Storage::delete("images/orders/{$id}" . $bukti->meta_value);
            }

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update order status']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function generateOrderNR()
    {
        $orderObj = DB::table('orders')->select('order_number')->latest('id')->first();
        if ($orderObj) {
            $orderNumber = $orderObj->order_number;
            $removed1char = substr($orderNumber, 1);
            $generateOrder_nr = $stpad = '#' . str_pad($removed1char + 1, 8, "0", STR_PAD_LEFT);
        } else {
            $generateOrder_nr = '#' . str_pad(1, 8, "0", STR_PAD_LEFT);
        }
        return $generateOrder_nr;
    }

    public function rupiah($angka)
    {
        $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
        return $hasil_rupiah;
    }
}
