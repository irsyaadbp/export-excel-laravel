<?php

namespace App\Http\Controllers;

use App\Mail\OtpForgotPasswordMail;
use App\Models\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Passport\Client as OClient;

class AuthController extends Controller
{

    public function list(Request $request)
    {
        try {
            // // Pagination
            $page = $request->get('page', 1);
            $per_page = $request->get('per_page', 10);
            $offset = $per_page * ($page - 1);

            // Search
            $search = $request->get('search', '');
            $role = $request->get('role', '');

            // Sorting
            $sort = $request->get('sort', 'updated_at');
            $order = $request->get('order', 'desc');


            $total = User::select('count(id)')
                ->where(function ($query) use ($search) {
                    $query->where('first_name', 'ilike', '%' . $search . '%')
                        ->orWhere('last_name', 'ilike', '%' . $search . '%')
                        ->orWhere('email', 'ilike', '%' . $search . '%')
                        ->orWhere('phone', 'ilike', '%' . $search . '%');
                })->when($role, function ($q) use ($role) {
                    return $q->where('role', 'LIKE', '%' . $role . '%');;
                })
                ->count();


            $preOrder = User::where(function ($query) use ($search) {
                $query->where('first_name', 'ilike', '%' . $search . '%')
                    ->orWhere('last_name', 'ilike', '%' . $search . '%')
                    ->orWhere('email', 'ilike', '%' . $search . '%')
                    ->orWhere('phone', 'ilike', '%' . $search . '%');
            })->when($role, function ($q) use ($role) {
                return $q->where('role', 'LIKE', '%' . $role . '%');;
            })
                ->orderBy($sort, $order)->skip($offset)
                ->take($per_page)
                ->get();


            $response = array(
                "success" => true,
                "total" => (int) $total,
                "page" => (int) $page,
                "per_page" => (int) $per_page,
                "data" => $preOrder
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }


    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [boolean] success
     * @return [string] message
     */
    public function registerUser(Request $request)
    {

        $data = $request->all();

        $validator = Validator::make($data, [
            'first_name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6',
            'confirm' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()], 422);
        }

        $user = new User(array_merge($data, [
            'password' => bcrypt($request->password),
            'role' => 'user'
        ]));
        if ($user->save()) {
            return response()->json([
                'success' => true,
                'message' => 'Successfully created user!'
            ], 201);
        } else {
            return response()->json(['success' => false, 'error' => 'Provide proper details']);
        }
    }

    public function registerAdmin(Request $request)
    {

        $data = $request->all();

        $validator = Validator::make($data, [
            'first_name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()], 422);
        }

        $user = new User(array_merge($data, [
            'password' => bcrypt($request->password),
            'role' => 'admin'
        ]));
        if ($user->save()) {
            return response()->json([
                'success' => true,
                'message' => 'Successfully created user!'
            ], 201);
        } else {
            return response()->json(['success' => false, 'error' => 'Provide proper details']);
        }
    }

    public function updateAdmin(Request $request, User $user)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            $id = $user->id;

            $validator = Validator::make($data, [
                'first_name' => 'required|string',
                'last_name' => 'string',
                'email' => ['required', 'string', 'email', Rule::unique('users')->ignore($id)],
                'password' => 'string|min:6',
                'phone' => 'numeric'
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }
            $dataEdit = $data;
            if ($request->has('profile_photo_url')) {
                if (gettype($data['profile_photo_url']) === 'string') {
                    $imageName = $data['profile_photo_url'];
                } else {
                    $today = time();
                    $imageName = str_replace(' ', '-', $data['first_name']) . $today . '.' . $data['profile_photo_url']->extension();

                    $data['profile_photo_url']->storeAs('images/user', $imageName);
                }
                $dataEdit = array_merge($data, ['profile_photo_url' => $imageName]);
            }

            if ($request->has('password')) {
                $dataEdit = array_merge($data, ['password' => bcrypt($request->password)]);
            }

            $dataEdit = array_merge($dataEdit, ['role' => 'admin']);

            DB::table('users')->where('id', $id)->update($dataEdit);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update admin']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function updatePhoto(Request $request)
    {
        $photo = $request->only('photo');
        $validator = Validator::make($photo, [
            'photo' => 'required|image|mimes:jpg,jpeg,png|max:1024'
        ]);
        $user = Auth::user();

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()], 422);
        }

        if ($request->hasFile('photo')) {
            $image_path = "images/avatars/" . $user->profile_photo_url;
            if (isset($user->profile_photo_url) && file_exists("images/avatars/" . $image_path)) {
                Storage::delete($image_path);
            }

            $fullName = $user->first_name . " " . $user->last_name;
            $today = time();
            $imageName = str_replace(' ', '-', $fullName) . $today . '.' . $request->photo->extension();
            $request->photo->storeAs('images/avatars', $imageName);

            User::where('id', $user->id)
                ->update(['profile_photo_url' => $imageName]);

            return response()->json(['success' => true, 'message' => 'Success update photo profile']);
        }
    }

    public function deletePhoto(Request $request)
    {
        $user = Auth::user();
        if (isset($user->profile_photo_url) && file_exists("images/avatars/" . $user->profile_photo_url)) {
            Storage::delete("images/avatars/" . $user->profile_photo_url);
        }

        User::where('id', $user->id)
            ->update(['profile_photo_url' => null]);

        return response()->json(['success' => true, 'message' => 'Success delete photo profile']);
    }

    public function updateProfile(Request $request)
    {
        DB::beginTransaction();
        $user = Auth::user();
        try {
            $data = $request->only('first_name', 'last_name', 'email', 'phone');
            $id = $user->id;

            $validator = Validator::make($data, [
                'first_name' => 'required|max:255',
                'last_name' => 'max:255',
                'email' => ['max:255', 'email', Rule::unique('users')->ignore($id)],
                'phone' => ['numeric', Rule::unique('users')->ignore($id)],
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }


            DB::table('users')->where('id', $id)->update($data);
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update profile']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }


    public function updatePassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('current_password', 'new_password', 'confirm_new_password');

            $validator = Validator::make($data, [
                'current_password' => 'required|string|min:6',
                'new_password' => 'required|string|min:6',
                'confirm_new_password' => 'required|same:new_password',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $userLoged = Auth::user();
            if (Hash::check($data['current_password'], $userLoged->password)) {
                $userId = $userLoged->id;

                DB::table('users')->where('id', $userId)->update(['password' => bcrypt($data['new_password'])]);
            } else {
                return response()->json(['success' => false, 'message' => "Old password not valid", 'error' => array('current_password' => ["The Old Password field not valid"])], 400);
            }
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update profile']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function loginUser(Request $request)
    {
        $credentials = request(['email', 'password']);
        $validator = Validator::make($credentials, [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()], 422);
        }
        $credentials = request(['email', 'password']);
        if (!Auth::attempt(array_merge($credentials, ['role' => 'user']))) {
            return response()->json([
                'success' => false,
                'message' => 'Please login with valid account'
            ], 401);
        }

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        DB::table('users')->where('id', $user->id)->update(['remember_token' => null]);

        return response()->json([
            'success' => true,
            'access_token' => $tokenResult->plainTextToken,
            'data' => $user,
        ]);
    }

    public function loginAdmin(Request $request)
    {
        $credentials = request(['email', 'password']);


        $validator = Validator::make($credentials, [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()], 422);
        }
        $credentials = request(['email', 'password']);
        if (!Auth::attempt(array_merge($credentials, ['role' => 'admin']))) {
            return response()->json([
                'success' => false,
                'message' => 'Please login with valid account admin'
            ], 401);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        return response()->json([
            'success' => true,
            'access_token' => $tokenResult->plainTextToken,
            'data' => $user,
        ]);
    }

    public function delete(Request $request, User $user)
    {
        DB::beginTransaction();

        try {
            if (isset($user->profile_photo_url) && file_exists("images/avatars/" . $user->profile_photo_url)) {
                unlink("images/avatars/" . $user->profile_photo_url);
            }

            DB::table('users')->delete($user->id);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success delete user']);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function requestForgotPassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('email');
            $validator = Validator::make($data, [
                'email' => 'required|string|email',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $user = DB::table('users')->where('email', $data['email']);

            if (!$user->count()) {
                return response()->json(['success' => false, 'message' => 'User not found'], 404);
            }

            $otp = random_int(100000, 999999);
            $user = $user->first();

            $config_email = DB::table('configs')->where('meta_key', 'config_email_receiver')->first();
            $config_no_wa = DB::table('configs')->where('meta_key', 'config_no_wa')->first();

            $details = [
                'user' => $user,
                'otp' => $otp,
                'config' => [
                    'config_email' => $config_email->meta_value,
                    'config_no_wa' => $config_no_wa->meta_value
                ]
            ];

            $to = $data['email'];

            Mail::to($to)->send(new OtpForgotPasswordMail($details));

            DB::table('users')->where('email', $data['email'])->update(['remember_token' => Hash::make($otp)]);

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success request forgot password']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function verifyForgotPassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('email', 'otp');
            $validator = Validator::make($data, [
                'email' => 'required|string|email',
                'otp' => 'required|numeric|digits:6',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $user = User::where('email', $data['email']);

            if (!$user->count()) {
                return response()->json(['success' => false, 'message' => 'User not found'], 404);
            }

            $user = $user->first()->setHidden(['password'])->setVisible(['id', 'email', 'first_name', 'last_name', 'remember_token']);

            if (!Hash::check($data['otp'], $user->remember_token)) {
                return response()->json(['success' => false, 'message' => 'OTP not match'], 400);
            }
            // DB::table('users')->where('email', $data['email'])->update(['remember_token' => null]);

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success request forgot password']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function resetPassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('email', 'otp', 'new_password', 'confirm_password');

            $validator = Validator::make($data, [
                'email' => 'required|string|email',
                'otp' => 'required|numeric|digits:6',
                'new_password' => 'required|string|min:6',
                'confirm_password' => 'required|same:new_password',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $user = User::where('email', $data['email']);

            if (!$user->count()) {
                return response()->json(['success' => false, 'message' => 'User not found'], 404);
            }

            $user = $user->first()->setHidden(['password'])->setVisible(['id', 'email', 'first_name', 'last_name', 'remember_token']);

            if (!Hash::check($data['otp'], $user->remember_token)) {
                return response()->json(['success' => false, 'message' => 'OTP not match'], 400);
            }
            DB::table('users')->where('id', $user->id)->update(['password' => Hash::make($data['new_password']), 'remember_token' => null]);
            
            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success update profile']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json(['success' => true, 'data' => $request->user()]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [boolean] success
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json([
            'success' => true,
            'message' => 'Successfully logged out'
        ]);
    }
}
