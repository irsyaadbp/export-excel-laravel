<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AddressUserController extends Controller
{
    public function list(Request $request)
    {
        try {
            $user = Auth::user();
            $address = Address::where('user_id', $user->id)->orderBy('default', 'desc')
                ->get();

            $response = array(
                "success" => true,
                "data" => $address
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function searchPlaces(Request $request)
    {
        try {
            $search = $request->get('search', '');
            $locations = Http::acceptJson()->get(env('MIX_MAP_URL', '') . '/place/autocomplete/json', [
                'language' => 'id',
                'components' => 'country:id',
                'key' => env('MIX_API_KEY_GOOGLE', ''),
                'input' => $search
            ]);

            $response = array(
                "success" => true,
                "data" => $locations->json()
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }
    public function reverseLatLong(Request $request)
    {
        try {
            $lat = $request->get('lat', 0.0);
            $long = $request->get('long', 0.0);
            $place_id = $request->get('place_id', '');

            $param = array(
                'language' => 'id',
                'key' => env('MIX_API_KEY_GOOGLE', ''),
            );

            if (!empty($place_id)) {
                $locations = Http::acceptJson()->get(env('MIX_MAP_URL', '') . '/geocode/json', [
                    'language' => 'id',
                    'key' => env('MIX_API_KEY_GOOGLE', ''),
                    'place_id' => $place_id
                ]);
            }

            if (!empty($lat) && !empty($long)) {
                $locations = Http::acceptJson()->get(env('MIX_MAP_URL', '') . '/geocode/json', [
                    'language' => 'id',
                    'key' => env('MIX_API_KEY_GOOGLE', ''),
                    'latlng' => "{$lat},{$long}"
                ]);
            }

            $response = array(
                "success" => true,
                "data" => $locations->json(),
            );

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['success' => false, "message" => $e->getMessage()], 400);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();

        try {
            $data = $request->only('name', 'label', 'lat', 'long', 'phone', 'address', 'note', 'formatted_address');

            $validator = Validator::make($data, [
                'name' => 'required|max:255',
                'label' => 'required|max:255',
                'lat' => 'required',
                'long' => 'required',
                'phone' => 'required|max:255',
                'address' => 'required',
                'note' => 'max:255',
            ]);

            $user = Auth::user();

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $addressUnique = DB::table('addresses')->where('user_id', $user->id)->where('label', 'ilike', $data['label'])->first();

            if (isset($addressUnique)) {
                return response()->json(['success' => false, 'error' => [
                    'label' => ["The label has already been taken."],
                ]], 422);
            }

            $addressDefault = DB::table('addresses')->where('user_id', $user->id)->where('default', 1)->first();

            if (!isset($addressDefault)) {
                $data = array_merge($data, ['default' => 1]);
            }

            $data = array_merge($data, ['user_id' => $user->id]);

            DB::table('addresses')->insert($data);

            // all good
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Success create address'], 201);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request, Address $address)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('name', 'label', 'lat', 'long', 'phone', 'address', 'note');
            $id = $address->id;
            $user = Auth::user();

            $validator = Validator::make($data, [
                'name' => 'max:255',
                'label' => 'max:255',
                'phone' => 'max:255',
                'address' => 'max:255',
                'note' => 'max:255',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()], 422);
            }

            $addressUnique = DB::table('addresses')->where('user_id', $user->id)->where('label', 'ilike', $data['label'])->first();

            if (isset($addressUnique) && $addressUnique->id !== $id) {
                return response()->json(['success' => false, 'error' => [
                    'label' => ["The label has already been taken."],
                ]], 422);
            }

            if (isset($address)) {

                if ($address->user_id === $user->id) {
                    DB::table('addresses')->where('id', $id)->update($data);

                    // all good
                    DB::commit();
                    return response()->json(['success' => true, 'message' => 'Success update address']);
                }

                return response()->json(['success' => false, 'message' => 'User not valid'], 400);
            }

            return response()->json(['success' => false, 'message' => 'Address not found'], 404);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function updateDefault(Request $request, Address $address)
    {
        DB::beginTransaction();
        try {
            $id = $address->id;

            if (isset($address)) {
                $user = Auth::user();
                if ($address->user_id === $user->id) {
                    DB::table('addresses')->where('user_id', $user->id)->update(['default' => 0]);
                    DB::table('addresses')->where('id', $id)->update(['default' => 1]);

                    // all good
                    DB::commit();
                    return response()->json(['success' => true, 'message' => 'Success change default address']);
                }

                return response()->json(['success' => false, 'message' => 'User not valid'], 400);
            }

            return response()->json(['success' => false, 'message' => 'Address not found'], 404);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function delete(Request $request, Address $address)
    {
        DB::beginTransaction();

        try {

            if (isset($address)) {
                $user = Auth::user();
                if ($address->user_id === $user->id) {
                    DB::table('addresses')->delete($address->id);

                    // all good
                    DB::commit();
                    return response()->json(['success' => true, 'message' => 'Success delete address']);
                }

                return response()->json(['success' => false, 'message' => 'User not valid'], 400);
            }
            return response()->json(['success' => false, 'message' => 'Address not found'], 404);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}
