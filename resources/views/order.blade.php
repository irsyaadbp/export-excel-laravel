<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    </style>
</head>
<body>
<tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>

<table style="">
        <tr>
            <th></th>
            <th>Order Number</th>
            <th>Customer Name</th>
            <th>Phone Number</th>
            <th>Order For</th>
            <th>Delivery Type</th>
            <th>Order Detail</th>
            <th>Gift Card</th>
            <th>Price</th>
            <th>Status</th>
            <th>Created Date</th>
        </tr>
        
    @foreach ($order as $key=>$item)
        <tr>
            <td></td>
            <td>{{ $item->order_number }}</td>
            <td>{{ $item->user_detail->first_name }} {{ $item->user_detail->last_name }}</td>
            <td>{{ $item->user_detail->phone }}</td>
            <td>{{ $item->order_date }}</td>
            <td>{{ $item->delivery_type }}</td>
            <td>
                @foreach($item->order_item as $okey=>$oitem)
                <p>Product : {{ $oitem->product_name }}</p>
                <p>Qty {{ $oitem->qty }}</p>
                    @foreach($oitem->order_item_variants as $vkey=>$vitem)
                    <p>- {{ $vitem->variant_name }} : {{ $vitem->variant_item_name }}</p>
                    @endforeach
                @endforeach
            </td>
            <td>
                @foreach($item->order_item as $okey=>$oitem)
                <p>Product : {{ $oitem->product_name }}</p>
                    @if(isset($oitem->order_item_meta[0]))
                        <p>=> {{ $oitem->order_item_meta[0]->meta_value }}</p>
                    @else
                        <p>=> {{ "No gift card" }}</p>
                    @endif
                <br>
                @endforeach
            </td>
            <td>{{ $item->total_price }}</td>
            <td>{{ $item->status }}</td>
            <td>{{ $item->created_at }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>