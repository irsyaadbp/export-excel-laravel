import { Ability } from '@casl/ability'
import Vue from 'vue'
import useJwt from '@/auth/jwt/useJwt'
import { initialAbility } from './config'

//  Read ability from localStorage
// * Handles auto fetching previous abilities if already logged in user
// ? You can update this if you store user abilities to more secure place
// ! Anyone can update localStorage so be careful and please update this
const userData = JSON.parse(Vue.$cookies.get(useJwt.jwtConfig.userDataKey))
const existingAbility = userData ? userData.ability : null

export default new Ability(existingAbility || initialAbility)
