import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import useJwt from '@/auth/jwt/useJwt'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior() {
        return { x: 0, y: 0 }
    },
    routes: [
        // Landing Route
        {
            path: '/',
            name: 'home',
            component: () => import('@/views/landing/Home.vue'),
            meta: {
                pageTitle: 'Home',
            },
        },
        {
            path: '/page/:slug',
            name: 'pages',
            component: () => import('@/views/landing/Page.vue'),
            meta: {
                pageTitle: 'Page',
            },
        },
        {
            path: '/pre-order/:slug',
            name: 'pre-order',
            component: () => import('@/views/landing/PreOrder.vue'),
            meta: {
                pageTitle: 'Pre Order',
                contentClass: 'ecommerce-application',
            },
        },
        {
            path: '/cart',
            name: 'cart',
            component: () => import('@/views/landing/Cart.vue'),
            meta: {
                pageTitle: 'Cart',
                contentClass: 'ecommerce-application',
                mustLogin: true,
            },
        },
        {
            path: '/checkout',
            name: 'checkout',
            component: () => import('@/views/landing/Checkout.vue'),
            meta: {
                pageTitle: 'Checkout',
                contentClass: 'ecommerce-application',
                mustLogin: true,
            },
        },
        {
            path: '/profile',
            name: 'profile',
            component: () => import('@/views/landing/profile'),
            meta: {
                pageTitle: 'Profile',
                mustLogin: true,
            },
        },
        {
            path: '/order-history',
            name: 'order-history',
            component: () => import('@/views/landing/OrderHistory.vue'),
            meta: {
                mustLogin: true,
            },
        },
        {
            path: '/order-history/detail/:orderNumber',
            name: 'order-history.detail',
            component: () => import('@/views/landing/OrderDetail.vue'),
            meta: {
                mustLogin: true,
            },
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('@/views/Login.vue'),
            meta: {
                layout: 'full',
            },
        },
        {
            path: '/forgot-password',
            name: 'forgot-password',
            component: () => import('@/views/admin/ForgotPassword.vue'),
            meta: {
                layout: 'full',
            },
        },
        {
            path: '/register',
            name: 'register',
            component: () => import('@/views/Register.vue'),
            meta: {
                layout: 'full',
            },
        },
        // Dashboard Route
        {
            path: '/admin',
            name: 'dashboard',
            component: () => import('@/views/admin/Dashboard.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Home',
                breadcrumb: [
                    {
                        text: 'Home',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/product/list',
            name: 'dashboard.product.list',
            component: () => import('@/views/admin/product/List.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Product List',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Product',
                        to: '/admin/product/list',
                    },
                    {
                        text: 'List',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/product/create',
            name: 'dashboard.product.create',
            component: () => import('@/views/admin/product/Mutation.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Create Product',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Product',
                        to: '/admin/product/list',
                    },
                    {
                        text: 'Create',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/product/edit/:id',
            name: 'dashboard.product.edit',
            component: () => import('@/views/admin/product/Mutation.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Edit Product',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Product',
                        to: '/admin/product/list',
                    },
                    {
                        text: 'Edit',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/product/:id/variant/edit',
            name: 'dashboard.product.variant.edit',
            component: () =>
                import(
                    '@/views/admin/product/variant/MutationVariantItems.vue'
                ),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Edit Product Variant',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Product',
                        to: '/admin/product/list',
                    },
                    {
                        text: 'Edit Variant',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/product/variant/list',
            name: 'dashboard.product.variant.list',
            component: () => import('@/views/admin/product/variant/List.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Variant List',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Product',
                        to: '/admin/product/list',
                    },
                    {
                        text: 'Variant',
                        to: '/admin/product/variant/list',
                    },
                    {
                        text: 'List',
                        active: true,
                    },
                ],
            },
        },
        // PRE ORDER
        {
            path: '/admin/pre-order/list',
            name: 'dashboard.preorder.list',
            component: () => import('@/views/admin/pre-order/List.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Pre Order List',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Pre Order',
                        to: '/admin/pre-order/list',
                    },
                    {
                        text: 'List',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/pre-order/create',
            name: 'dashboard.preorder.create',
            component: () => import('@/views/admin/pre-order/Mutation.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Create Pre Order',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Pre Order',
                        to: '/admin/pre-order/list',
                    },
                    {
                        text: 'Create',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/pre-order/edit/:id',
            name: 'dashboard.preorder.edit',
            component: () => import('@/views/admin/pre-order/Mutation.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Edit Pre Order',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Pre Order',
                        to: '/admin/pre-order/list',
                    },
                    {
                        text: 'Edit',
                        active: true,
                    },
                ],
            },
        },
        // ORDER
        {
            path: '/admin/order',
            name: 'dashboard.order',
            component: () => import('@/views/admin/order/List.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Order List',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Order',
                        to: '/admin/order',
                    },
                    {
                        text: 'List',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/order/:orderNumber',
            name: 'dashboard.order.detail',
            component: () => import('@/views/admin/order/Detail.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Order Detail',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Order',
                        to: '/admin/order',
                    },
                    {
                        text: 'Detail',
                        active: true,
                    },
                ],
            },
        },
        // USER
        {
            path: '/admin/user/customer',
            name: 'dashboard.user.customer',
            component: () => import('@/views/admin/user/Customer.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Customer List',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Customer',
                        to: '/admin/user/customer',
                    },
                    {
                        text: 'List',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/user/admin',
            name: 'dashboard.user.admin',
            component: () => import('@/views/admin/user/Admin.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Admin List',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Admin',
                        to: '/admin/user/admin',
                    },
                    {
                        text: 'List',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/page/list',
            name: 'dashboard.page.list',
            component: () => import('@/views/admin/pages/List.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Page List',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Page',
                        to: '/admin/page/list',
                    },
                    {
                        text: 'List',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/page/create',
            name: 'dashboard.page.create',
            component: () => import('@/views/admin/pages/Mutation.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Create Page',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Page',
                        to: '/admin/page/list',
                    },
                    {
                        text: 'Create',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/page/edit/:id',
            name: 'dashboard.page.edit',
            component: () => import('@/views/admin/pages/Mutation.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Edit Page',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Page',
                        to: '/admin/page/list',
                    },
                    {
                        text: 'Edit',
                        active: true,
                    },
                ],
            },
        },
        // SETTING
        {
            path: '/admin/setting/banner',
            name: 'dashboard.setting.banner',
            component: () => import('@/views/admin/setting/Banner.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Banner Setting',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Setting',
                        to: '/admin/setting/config',
                    },
                    {
                        text: 'Banner',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/setting/payment',
            name: 'dashboard.setting.payment',
            component: () => import('@/views/admin/setting/Payment.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Payment Methods',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Setting',
                        to: '/admin/setting/config',
                    },
                    {
                        text: 'Payment',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/setting/menu',
            name: 'dashboard.setting.menu',
            component: () => import('@/views/admin/setting/Menu.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Menu',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Setting',
                        to: '/admin/setting/config',
                    },
                    {
                        text: 'Menu',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/setting/config',
            name: 'dashboard.setting.config',
            component: () => import('@/views/admin/setting/Config.vue'),
            meta: {
                layout: 'vertical',
                mustLogin: true,
                mustAdmin: true,
                pageTitle: 'Configuration',
                breadcrumb: [
                    {
                        text: 'Home',
                        to: '/admin',
                    },
                    {
                        text: 'Setting',
                        to: '/admin/setting/config',
                    },
                    {
                        text: 'Configuration',
                        active: true,
                    },
                ],
            },
        },
        {
            path: '/admin/login',
            name: 'dashboard.login',
            component: () => import('@/views/admin/Login.vue'),
            meta: {
                layout: 'full',
            },
        },
        // ERROR
        {
            path: '/error-404',
            name: 'error-404',
            component: () => import('@/views/error/Error404.vue'),
            meta: {
                layout: 'full',
            },
        },
        {
            path: '*',
            redirect: 'error-404',
        },
    ],
})

router.beforeEach(async (to, from, next) => {
    const hasToken = useJwt.getToken() !== null
    const mustLogin = to.meta?.mustLogin
    const mustAdmin = to.meta?.mustAdmin

    if (hasToken) {
        try {
            const resp = await useJwt.profile()
            const { data } = resp.data

            store.set('user/userData', data)

            if (data?.role !== 'admin' && mustAdmin && mustLogin) {
                return next({
                    path: '/',
                })
            }

            if (data?.role && (to.name === 'login' || to.name === 'register' || to.name === 'forgot-password')) {
                return next({
                    path: '/',
                })
            }

            if (data?.role === 'admin' && to.name === 'dashboard.login') {
                return next({
                    path: '/admin',
                })
            }

            if (data?.role === 'user' && to.name === 'dashboard.login') {
                return next({
                    path: '/',
                })
            }

            return next()
        } catch (err) {
            return next({
                path: '/login',
                query: { redirect: to.fullPath },
            })
        }
    } else {
        if (mustLogin) {
            if (to.name.includes('dashboard')) {
                return next({
                    path: '/admin/login',
                    query: { redirect: to.fullPath },
                })
            }
            return next({
                path: '/login',
                query: { redirect: to.fullPath },
            })
        }

        return next()
    }
})

// ? For splash screen
// Remove afterEach hook if you are not using splash screen
router.afterEach(() => {
    // Remove initial loading
    const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = 'none'
    }
})

export default router
