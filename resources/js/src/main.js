import Vue from 'vue'
import { ToastPlugin, ModalPlugin, BootstrapVue } from 'bootstrap-vue'
import VueCompositionAPI from '@vue/composition-api'
import routeMixins from '@core/mixins/ui/route'
import globalMixins from '@core/mixins/ui/utils'
// import VueGoogleMap from 'vuejs-google-maps'
// import 'vuejs-google-maps/dist/vuejs-google-maps.css'

import router from './router'
import store from './store'
import App from './App.vue'

// Global Components
import './global-components'

// 3rd party plugins
import '@/libs/portal-vue'
import '@/libs/sweet-alerts'
import '@/libs/vue-select'
import '@/libs/cookies'
import '@/libs/clipboard'

// BSV Plugin Registration
Vue.use(BootstrapVue)
Vue.use(ToastPlugin)
Vue.use(ModalPlugin)

// Composition API
Vue.use(VueCompositionAPI)

// Vue.use(VueGoogleMap, {
//     load: {
//         apiKey: process.env.MIX_API_KEY_GOOGLE,
//     },
// })
Vue.mixin(routeMixins)
Vue.mixin(globalMixins)

// Feather font icon - For form-wizard
// * Shall remove it if not using font-icons of feather-icons - For form-wizard
require('@core/assets/fonts/feather/iconfont.css') // For form-wizard

// import core styles
require('@core/scss/core.scss')

// import assets styles
require('@/assets/scss/style.scss')

Vue.config.productionTip = false

new Vue({
    store,
    router,
    render: h => h(App),
}).$mount('#app')
