import { defaultMutations } from 'vuex-easy-access'
import axios from '@axios'

const state = () => ({
    datas: [],
    loading: false,
    pagination: {
        page: 1,
        total: 0,
        per_page: 10,
    },
    error: {},
    show_alert: false,
    alert_title: '',
    alert_message: '',
    alert_status: 'success',
})

export default {
    namespaced: true,
    state,
    getters: {},
    mutations: {
        ...defaultMutations(state()),
    },
    actions: {
        load({ dispatch }, params) {
            dispatch('set/loading', true)
            return axios
                .get('/api/customer/config', { params })
                .then(res => {
                    dispatch('set/datas', res?.data?.data)
                    return res?.data?.data
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error get config')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )
                    return false
                })
                .finally(() => dispatch('set/loading', false))
        },
        
        updateBulk({ dispatch }, params) {
            return axios
                .post(`/api/customer/config/bulk-update`, params)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch('set/alert_message', 'Config successfully updated!')
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error edit config')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
    },
}
