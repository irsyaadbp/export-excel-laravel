import { defaultMutations } from 'vuex-easy-access'
import axios from '@axios'

const state = () => ({
    datas: [],
    detail: {},
    loading: false,
    error: {},
    show_alert: false,
    alert_title: '',
    alert_message: '',
    alert_status: 'success',
})

export default {
    namespaced: true,
    state,
    getters: {},
    mutations: {
        ...defaultMutations(state()),
    },
    actions: {
        getPageDetail({ dispatch }, slug) {
            dispatch('set/loading', true)
            return axios
                .get(`/api/customer/page/detail`, { params: { slug } })
                .then(res => {
                    const detail = res?.data?.data
                    dispatch('set/detail', detail)
                    return detail
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error get page')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return err.response
                })
                .finally(() => dispatch('set/loading', false))
        },
    },
}
