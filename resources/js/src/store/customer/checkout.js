import { defaultMutations } from 'vuex-easy-access'
import axios from '@axios'

const state = () => ({
    datas: [],
    methods: [],
    configs: [],
    loading: false,
    error: {},
    show_alert: false,
    alert_title: '',
    alert_message: '',
    alert_status: 'success',
})

export default {
    namespaced: true,
    state,
    getters: {},
    mutations: {
        ...defaultMutations(state()),
    },
    actions: {
        load({ dispatch }, params) {
            dispatch('set/loading', true)
            return axios
                .get('/api/customer/cart/checkout', { params })
                .then(res => {
                    dispatch('set/datas', res?.data?.data)
                    return res?.data?.data
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error get checkout item')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )
                    return false
                })
                .finally(() => dispatch('set/loading', false))
        },
        loadMethods({ dispatch }, params) {
            dispatch('set/loading', true)
            return axios
                .get('/api/customer/payment-methods', { params })
                .then(res => {
                    dispatch('set/methods', res?.data?.data)
                    return res?.data?.data
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error get payment methods')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )
                    return false
                })
                .finally(() => dispatch('set/loading', false))
        },
        loadConfig({ dispatch }, params) {
            dispatch('set/loading', true)
            return axios
                .get('/api/customer/cart/checkout/config', { params })
                .then(res => {
                    dispatch('set/configs', res?.data?.data)
                    return res?.data?.data
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error get config')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )
                    return false
                })
                .finally(() => dispatch('set/loading', false))
        },
        getVariantItems(__, params) {
            return axios
                .get('/api/customer/cart/variant-item', { params })
                .then(res => res?.data?.data)
                .catch(err => ({
                    message: err?.response?.data?.message || err?.message,
                    error: err?.response?.data?.error,
                }))
        },
    },
}
