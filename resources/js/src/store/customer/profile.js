import { defaultMutations } from 'vuex-easy-access'
import axios from '@axios'

const state = () => ({
    datas: [],
    loading: false,
    error: {},
    show_alert: false,
    alert_title: '',
    alert_message: '',
    alert_status: 'success',
})

export default {
    namespaced: true,
    state,
    getters: {},
    mutations: {
        ...defaultMutations(state()),
    },
    actions: {
        updateProfile({ dispatch }, params) {
            return axios
                .post(`/api/auth/update/profile`, params)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch(
                        'set/alert_message',
                        'Profile successfully updated!'
                    )
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error edit profile')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
        changePassword({ dispatch }, params) {
            return axios
                .post(`/api/auth/update/password`, params)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch(
                        'set/alert_message',
                        'Password successfully updated!'
                    )
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error edit photo password')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
        updatePhoto({ dispatch }, params) {
            return axios
                .post(`/api/auth/update/avatar`, params)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch(
                        'set/alert_message',
                        'Photo Profile successfully updated!'
                    )
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error edit photo profile')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
        deletePhoto({ dispatch }) {
            return axios
                .delete(`/api/auth/delete/avatar`)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch(
                        'set/alert_message',
                        'Photo Profile successfully deleted!'
                    )
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error delete photo profile')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
    },
}
