import { defaultMutations } from 'vuex-easy-access'
import axios from '@axios'

const state = () => ({
    datas: [],
    detail: {},
    loading: false,
    pagination: {
        page: 1,
        total: 0,
        per_page: 10,
    },
    error: {},
    show_alert: false,
    alert_title: '',
    alert_message: '',
    alert_status: 'success',
})

export default {
    namespaced: true,
    state,
    getters: {},
    mutations: {
        ...defaultMutations(state()),
    },
    actions: {
        load({ dispatch }, params) {
            dispatch('set/loading', true)
            return axios
                .get('/api/admin/order', { params })
                .then(res => {
                    dispatch('set/datas', res?.data?.data)
                    dispatch('set/pagination', {
                        page: +res?.data?.page || 1,
                        total: +res?.data?.total || 0,
                        per_page: +res?.data?.per_page || 10,
                    })
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error get variant')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )
                    return false
                })
                .finally(() => dispatch('set/loading', false))
        },
        loadById({ dispatch }, orderNumber) {
            dispatch('set/loading', true)
            return axios
                .get(`/api/admin/order/detail`, { params: { orderNumber } })
                .then(res => {
                    dispatch('set/detail', res?.data?.data)

                    return res?.data?.data
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error get order detail')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )
                    return false
                })
                .finally(() => dispatch('set/loading', false))
        },
        updateStatus({ dispatch }, params) {
            return axios
                .post(`/api/admin/order/update-status/${params?.id}`, params)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch(
                        'set/alert_message',
                        'Order status successfully updated!'
                    )
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error update status order')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
        downloadImage({ dispatch }, orderId) {
            return axios
                .get(`/api/admin/order/download-image/${orderId}`, {
                    responseType: 'arraybuffer',
                })
                .then(res => {
                    console.log(res.data)
                    const disposition = res.request.getResponseHeader(
                        'Content-Disposition'
                    )
                    let fileName = ''
                    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/
                    const matches = filenameRegex.exec(disposition)
                    if (matches != null && matches[1]) {
                        fileName = matches[1].replace(/['"]/g, '')
                    }
                    const blob = new Blob([res.data], {
                        type: 'application/zip',
                    })

                    const downloadUrl = URL.createObjectURL(blob)
                    const a = document.createElement('a')
                    a.href = downloadUrl
                    a.download = fileName
                    document.body.appendChild(a)
                    a.click()
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch(
                        'set/alert_message',
                        'Images successfully downloaded!'
                    )
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error download images')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
        deleteImage({ dispatch }, orderId) {
            return axios
                .delete(`/api/admin/order/delete-image/${orderId}`)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch(
                        'set/alert_message',
                        'Images successfully deleted!'
                    )
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error delete images')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
    },
}
