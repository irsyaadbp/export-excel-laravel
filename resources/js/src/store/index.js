import Vue from 'vue'
import Vuex from 'vuex'

// Plugins
import createEasyAccess from 'vuex-easy-access'

// Modules
import app from './app'
import appConfig from './app-config'
import verticalMenu from './vertical-menu'
import user from './user'
import productAdmin from './product-admin'
import variantAdmin from './variant-admin'
import preorderAdmin from './preorder-admin'
import userAdmin from './user-admin'
import configAdmin from './config-admin'
import bannerAdmin from './banner-admin'
import paymentAdmin from './payment-admin'
import pageAdmin from './page-admin'
import orderAdmin from './order-admin'

// CUSTOMER
import configCustomer from './customer/config'
import profileCustomer from './customer/profile'
import addressCustomer from './customer/address'
import preorderCustomer from './customer/preorder'
import cartCustomer from './customer/cart'
import checkoutCustomer from './customer/checkout'
import paymentCustomer from './customer/payment'
import orderCustomer from './customer/order'
import pageCustomer from './customer/page'
import passwordCustomer from './customer/password'

const easyAccess = createEasyAccess()

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [easyAccess],
    modules: {
        app,
        appConfig,
        verticalMenu,
        user,
        productAdmin,
        variantAdmin,
        preorderAdmin,
        userAdmin,
        configAdmin,
        bannerAdmin,
        paymentAdmin,
        pageAdmin,
        configCustomer,
        profileCustomer,
        addressCustomer,
        preorderCustomer,
        cartCustomer,
        checkoutCustomer,
        paymentCustomer,
        orderCustomer,
        pageCustomer,
        orderAdmin,
        passwordCustomer
    },
    strict: process.env.DEV,
})
