import { defaultMutations } from 'vuex-easy-access'

const state = () => ({
    userData: {},
})

export default {
    namespaced: true,
    state,
    getters: {},
    mutations: {
        ...defaultMutations(state()),
    },
    actions: {
        setUser({ dispatch }, params) {
            dispatch('set/userData', params)
        },
    },
}
