import { $themeConfig } from '@themeConfig'
import { defaultMutations } from 'vuex-easy-access'
import axios from '@axios'

const state = () => ({
    logo: {
        appLogoLight: $themeConfig.app.appLogoImage,
        appLogoDark: $themeConfig.app.appLogoImageDark,
        appLogoIcon: $themeConfig.app.appLogoImageSmall,
    },
    menu: [],
    banner: [],
    layout: {
        isRTL: $themeConfig.layout.isRTL,
        skin: localStorage.getItem('vuexy-skin') || $themeConfig.layout.skin,
        routerTransition: $themeConfig.layout.routerTransition,
        type: $themeConfig.layout.type,
        contentWidth: $themeConfig.layout.contentWidth,
        menu: {
            hidden: $themeConfig.layout.menu.hidden,
        },
        navbar: {
            type: $themeConfig.layout.navbar.type,
            backgroundColor: $themeConfig.layout.navbar.backgroundColor,
        },
        footer: {
            type: $themeConfig.layout.footer.type,
        },
    },
})
export default {
    namespaced: true,
    state,
    getters: {},
    mutations: {
        ...defaultMutations(state()),
        TOGGLE_RTL(state) {
            state.layout.isRTL = !state.layout.isRTL
            document.documentElement.setAttribute(
                'dir',
                state.layout.isRTL ? 'rtl' : 'ltr'
            )
        },
        UPDATE_SKIN(state, skin) {
            state.layout.skin = skin

            // Update value in localStorage
            localStorage.setItem('vuexy-skin', skin)

            // Update DOM for dark-layout
            if (skin === 'dark') document.body.classList.add('dark-layout')
            else if (document.body.className.match('dark-layout'))
                document.body.classList.remove('dark-layout')
        },
        UPDATE_ROUTER_TRANSITION(state, val) {
            state.layout.routerTransition = val
        },
        UPDATE_LAYOUT_TYPE(state, val) {
            state.layout.type = val
        },
        UPDATE_CONTENT_WIDTH(state, val) {
            state.layout.contentWidth = val
        },
        UPDATE_NAV_MENU_HIDDEN(state, val) {
            state.layout.menu.hidden = val
        },
        UPDATE_NAVBAR_CONFIG(state, obj) {
            Object.assign(state.layout.navbar, obj)
        },
        UPDATE_FOOTER_CONFIG(state, obj) {
            Object.assign(state.layout.footer, obj)
        },
    },
    actions: {
        loadLogo({ dispatch }, params) {
            return axios
                .get('/api/customer/config', {
                    params: { ...params, search: 'config_logo_' },
                })
                .then(res => {
                    const data = res?.data?.data || []
                    const logoLight = data.find(
                        config => config.meta_key === 'config_logo_light'
                    )?.meta_value
                    const logoDark = data.find(
                        config => config.meta_key === 'config_logo_dark'
                    )?.meta_value
                    const logoIcon = data.find(
                        config => config.meta_key === 'config_logo_icon'
                    )?.meta_value

                    const logo = {
                        appLogoLight:
                            logoLight || $themeConfig.app.appLogoImage,
                        appLogoDark:
                            logoDark || $themeConfig.app.appLogoImageDark,
                        appLogoIcon:
                            logoIcon || $themeConfig.app.appLogoImageSmall,
                    }

                    dispatch('set/logo', logo)
                    return logo
                })
                .catch(() => false)
        },
        loadMenu({ dispatch }, params) {
            return axios
                .get('/api/customer/config', {
                    params: {
                        per_page: 1000,
                        sort: 'meta_key',
                        order: 'asc',
                        ...params,
                        search: 'menu-',
                    },
                })
                .then(res => {
                    const data = res?.data?.data || []
                    const menuAll = data
                        .reduce((result, current) => {
                            const menuNumber = current.meta_key.split('_')[0]
                            const menuType = current.meta_key.split('_')[1]

                            const indexMenu = result.findIndex(
                                menu => menu.key === menuNumber
                            )
                            if (indexMenu > -1) {
                                // eslint-disable-next-line no-param-reassign
                                result[indexMenu].data = {
                                    ...result[indexMenu].data,
                                    [menuType]: current.meta_value,
                                }
                            } else {
                                result.push({
                                    key: menuNumber,
                                    data: { [menuType]: current.meta_value },
                                })
                            }
                            return result
                        }, [])
                        .filter(menu => !!+menu.data.active)
                        .map(menu => menu.data)

                    dispatch('set/menu', menuAll)
                    return menuAll
                })
                .catch(() => false)
        },
        loadBanner({ dispatch }, params) {
            return axios
                .get('/api/customer/banner', { params })
                .then(res => {
                    const banner = res?.data?.data || []
                    dispatch('set/banner', banner)
                    return banner
                })
                .catch(() => false)
        },
    },
}
