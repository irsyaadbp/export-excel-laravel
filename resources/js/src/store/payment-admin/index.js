import { defaultMutations } from 'vuex-easy-access'
import axios from '@axios'

const state = () => ({
    datas: [],
    loading: false,
    pagination: {
        page: 1,
        total: 0,
        per_page: 10,
    },
    error: {},
    show_alert: false,
    alert_title: '',
    alert_message: '',
    alert_status: 'success',
})

export default {
    namespaced: true,
    state,
    getters: {},
    mutations: {
        ...defaultMutations(state()),
    },
    actions: {
        load({ dispatch }, params) {
            dispatch('set/loading', true)
            return axios
                .get('/api/admin/payment', { params })
                .then(res => {
                    dispatch('set/datas', res?.data?.data)
                    dispatch('set/pagination', {
                        page: +res?.data?.page || 1,
                        total: +res?.data?.total || 0,
                        per_page: +res?.data?.per_page || 10,
                    })
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error get payment methods')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )
                    return false
                })
                .finally(() => dispatch('set/loading', false))
        },
        create({ dispatch }, params) {
            return axios
                .post('/api/admin/payment/create', params)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch('set/alert_message', 'Payment Method successfully created')
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error create payment method')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
        update({ dispatch }, params) {
            return axios
                .post(`/api/admin/payment/update/${params?.id}`, params.data)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch('set/alert_message', 'Payment Method successfully edited!')
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error edit payment')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
        active({ dispatch }, params) {
            const message = params.active ? 'activated' : 'non activate'
            return axios
                .post(`/api/admin/payment/update/active/${params?.id}`, params)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch(
                        'set/alert_message',
                        `Payment Methods successfully ${message}!`
                    )
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', `Error ${message} payment method`)
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
        delete({ dispatch }, params) {
            return axios
                .delete(`/api/admin/payment/${params}`)
                .then(__ => {
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Success')
                    dispatch('set/alert_status', 'success')
                    dispatch(
                        'set/alert_message',
                        'Payment Methods successfully deleted!'
                    )
                    return true
                })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        dispatch('set/error', err?.response?.data?.error)
                    }
                    dispatch('set/show_alert', true)
                    dispatch('set/alert_title', 'Error delete payment method')
                    dispatch('set/alert_status', 'danger')
                    dispatch(
                        'set/alert_message',
                        err?.response?.data?.message || err?.message
                    )

                    return false
                })
        },
    },
}
