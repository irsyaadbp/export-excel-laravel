export default {
    data() {
        return {
            optionNumber: {
                numeral: true,
                numeralDecimalMark: ',',
                delimiter: '.',
                numeralPositiveOnly: true,
            },
        }
    },
    computed: {
        loadingGlobal() {
            return this.$store.get('app/loadingGlobal')
        },
    },
    methods: {
        setLoading(val) {
            this.$store.set('app/loadingGlobal', val)
        },
        formatImage(img, path = 'products') {
            if (
                img &&
                typeof img === 'string' &&
                !img.includes('resources/js/src/assets')
            ) {
                return img.includes('http') ? img : `/storage/images/${path}/${img}`
            }
            return null
        },
        formatRupiah(number) {
            return new Intl.NumberFormat('id-ID', {
                style: 'currency',
                currency: 'IDR',
            }).format(+number)
        },
    },
}
