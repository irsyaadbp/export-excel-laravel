import useJwt from '@/auth/jwt/useJwt'

const route = {
    computed: {
        isAdminPage() {
            return (this.$route?.name || '').includes('dashboard')
        },
        userData() {
            return this.$store.get('user/userData')
        },
        isAdmin() {
            return this.userData?.role === 'admin'
        },
        isLoggedIn() {
            return !!useJwt.getToken()
        },
        logoGlobal(){
            return this.$store.get('appConfig/logo')
        }
    },
}

export default route
