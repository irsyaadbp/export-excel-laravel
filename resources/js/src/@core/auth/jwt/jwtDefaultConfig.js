export default {
    // Endpoints
    loginAdminEndpoint: '/api/auth/admin/login',
    loginEndpoint: '/api/auth/user/login',
    
    registerEndpoint: '/api/auth/user/register',
 
    logoutEndpoint: '/api/auth/logout',
    profileEndpoint: '/api/auth/me',

    // This will be prefixed in authorization header with token
    // e.g. Authorization: Bearer <token>
    tokenType: 'Bearer',

    // Value of this property will be used as key to store JWT token in storage
    storageTokenKeyName: 'mooncena:access',
    storageRefreshTokenKeyName: 'mooncena:refresh',

    userDataKey: 'mooncena:user',
}
