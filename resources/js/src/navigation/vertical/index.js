export default [
    {
        title: 'Home',
        route: 'dashboard',
        icon: 'HomeIcon',
    },
    {
        title: 'Product',
        icon: 'GiftIcon',
        children: [
            {
                title: 'Create New',
                route: 'dashboard.product.create',
                icon: 'PlusIcon',
            },
            {
                title: 'All List',
                route: 'dashboard.product.list',
                icon: 'CircleIcon',
            },
            {
                title: 'Variant',
                route: 'dashboard.product.variant.list',
                icon: 'CircleIcon',
            },
        ],
    },
    {
        title: 'Pre Order',
        icon: 'ShoppingCartIcon',
        children: [
            {
                title: 'Create New',
                route: 'dashboard.preorder.create',
                icon: 'PlusIcon',
            },
            {
                title: 'All List',
                route: 'dashboard.preorder.list',
                icon: 'CircleIcon',
            },
        ],
    },
    {
        title: 'Order',
        route: 'dashboard.order',
        icon: 'FileTextIcon',
    },
    {
        title: 'User',
        icon: 'UsersIcon',
        children: [
            {
                title: 'Customer',
                route: 'dashboard.user.customer',
                icon: 'CircleIcon',
            },
            {
                title: 'Admin',
                route: 'dashboard.user.admin',
                icon: 'CircleIcon',
            },
        ],
    },
    {
        title: 'Page',
        icon: 'FileIcon',
        children: [
            {
                title: 'Create New',
                route: 'dashboard.page.create',
                icon: 'PlusIcon',
            },
            {
                title: 'All List',
                route: 'dashboard.page.list',
                icon: 'CircleIcon',
            },
        ],
    },
    {
        title: 'Setting',
        icon: 'SettingsIcon',
        children: [
            {
                title: 'Banner',
                route: 'dashboard.setting.banner',
                icon: 'CircleIcon',
            },
            {
                title: 'Payment',
                route: 'dashboard.setting.payment',
                icon: 'CircleIcon',
            },
            {
                title: 'Menu',
                route: 'dashboard.setting.menu',
                icon: 'CircleIcon',
            },
            {
                title: 'Config',
                route: 'dashboard.setting.config',
                icon: 'CircleIcon',
            },
        ],
    },
]
