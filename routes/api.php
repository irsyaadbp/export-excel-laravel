<?php

use App\Http\Controllers\AddressUserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BannerAdminController;
use App\Http\Controllers\BannerUserController;
use App\Http\Controllers\CartUserController;
use App\Http\Controllers\ConfigAdminController;
use App\Http\Controllers\OrderAdminController;
use App\Http\Controllers\OrderUserController;
use App\Http\Controllers\PageAdminController;
use App\Http\Controllers\PageUserController;
use App\Http\Controllers\PaymentAdminController;
use App\Http\Controllers\PaymentUserController;
use App\Http\Controllers\PreOrderAdminController;
use App\Http\Controllers\PreOrderUserController;
use App\Http\Controllers\ProductAdminController;
use App\Http\Controllers\VariantAdminController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/order', [OrderAdminController::class, 'list']);
Route::get('/excel', [OrderAdminController::class, 'downloadExcel']);

Route::group(['prefix' => 'auth'], function () {
    Route::post('request-forgot-password', [AuthController::class, 'requestForgotPassword'])->middleware('throttle:10,360');
    Route::post('verify-forgot-password', [AuthController::class, 'verifyForgotPassword']);
    Route::post('reset-password', [AuthController::class, 'resetPassword']);
    
    Route::group(['prefix' => 'user'], function () {
        Route::post('register', [AuthController::class, 'registerUser']);
        Route::post('login', [AuthController::class, 'loginUser']);
    });

    Route::group(['prefix' => 'admin'], function () {
        Route::post('login', [AuthController::class, 'loginAdmin']);
        Route::post('create', [AuthController::class, 'registerAdmin'])->middleware(['auth:sanctum', 'auth.admin']);
    });

    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::get('me', [AuthController::class, 'user']);
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('update/avatar', [AuthController::class, 'updatePhoto']);
        Route::post('update/profile', [AuthController::class, 'updateProfile']);
        Route::post('update/password', [AuthController::class, 'updatePassword']);
        Route::delete('delete/avatar', [AuthController::class, 'deletePhoto']);
    });
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth:sanctum', 'auth.admin']], function () {
    Route::group(['prefix' => 'product'], function () {
        Route::get('/', [ProductAdminController::class, 'list']);
        Route::get('/{product:id}', [ProductAdminController::class, 'getById']);
        Route::get('/{product:id}/variant-item', [ProductAdminController::class, 'getVariantItem']);
        Route::post('create', [ProductAdminController::class, 'create']);
        Route::post('update/{product:id}', [ProductAdminController::class, 'update']);
        Route::post('update/{id}/variant-item', [ProductAdminController::class, 'updateVariantItem']);
        Route::delete('{product:id}', [ProductAdminController::class, 'delete']);
    });

    Route::group(['prefix' => 'order'], function () {
        Route::get('/', [OrderAdminController::class, 'list']);
        Route::get('/detail', [OrderAdminController::class, 'getDetail']);
        Route::post('update-status/{order:id}', [OrderAdminController::class, 'updateStatus']);
        Route::get('download-image/{order:id}', [OrderAdminController::class, 'downloadAllImage']);
        Route::delete('delete-image/{order:id}', [OrderAdminController::class, 'deleteAllImage']);
    });

    Route::group(['prefix' => 'variant'], function () {
        Route::get('/', [VariantAdminController::class, 'list']);
        Route::post('create', [VariantAdminController::class, 'create']);
        Route::post('update/{variant:id}', [VariantAdminController::class, 'update']);
        Route::delete('{variant:id}', [VariantAdminController::class, 'delete']);
    });

    Route::group(['prefix' => 'pre-order'], function () {
        Route::get('/', [PreOrderAdminController::class, 'list']);
        Route::get('/{preorder:id}', [PreOrderAdminController::class, 'getById']);
        Route::post('create', [PreOrderAdminController::class, 'create']);
        Route::post('update/{preorder:id}', [PreOrderAdminController::class, 'update']);
        Route::post('/update/active/{preorder:id}', [PreOrderAdminController::class, 'updateActive']);
        Route::delete('{preorder:id}', [PreOrderAdminController::class, 'delete']);
    });

    Route::group(['prefix' => 'page'], function () {
        Route::get('/', [PageAdminController::class, 'list']);
        Route::get('/{page:id}', [PageAdminController::class, 'getById']);
        Route::post('create', [PageAdminController::class, 'create']);
        Route::post('update/{page:id}', [PageAdminController::class, 'update']);
        Route::delete('{page:id}', [PageAdminController::class, 'delete']);
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', [AuthController::class, 'list']);
        Route::post('/admin/{user:id}', [AuthController::class, 'updateAdmin']);
        Route::delete('/{user:id}', [AuthController::class, 'delete']);
    });

    Route::group(['prefix' => 'banner'], function () {
        Route::get('/', [BannerAdminController::class, 'list']);
        Route::post('/create', [BannerAdminController::class, 'create']);
        Route::post('/update/{banner:id}', [BannerAdminController::class, 'update']);
        Route::post('/update/active/{banner:id}', [BannerAdminController::class, 'updateActive']);
        Route::delete('{banner:id}', [BannerAdminController::class, 'delete']);
    });

    Route::group(['prefix' => 'payment'], function () {
        Route::get('/', [PaymentAdminController::class, 'list']);
        Route::post('/create', [PaymentAdminController::class, 'create']);
        Route::post('/update/{payment:id}', [PaymentAdminController::class, 'update']);
        Route::post('/update/active/{payment:id}', [PaymentAdminController::class, 'updateActive']);
        Route::delete('{payment:id}', [PaymentAdminController::class, 'delete']);
    });


    Route::group(['prefix' => 'config'], function () {
        Route::get('/', [ConfigAdminController::class, 'list']);
        Route::post('/update', [ConfigAdminController::class, 'update']);
        Route::post('/bulk-update', [ConfigAdminController::class, 'updateBulk']);
        Route::delete('/delete', [ConfigAdminController::class, 'delete']);
    });
});

Route::group(['prefix' => 'customer'], function () {
    Route::get('banner', [BannerUserController::class, 'list']);
    Route::get('page/detail', [PageUserController::class, 'getDetail']);
    Route::get('payment-methods', [PaymentUserController::class, 'list'])->middleware('auth:sanctum');

    Route::group(['prefix' => 'config'], function () {
        Route::get('/', [ConfigAdminController::class, 'list']);
        Route::post('/bulk-update', [ConfigAdminController::class, 'updateBulk'])->middleware('auth:sanctum');
    });

    Route::group(['prefix' => 'address', 'middleware' => ['auth:sanctum']], function () {
        Route::get('/', [AddressUserController::class, 'list']);
        Route::get('/search-places', [AddressUserController::class, 'searchPlaces']);
        Route::get('/reverse', [AddressUserController::class, 'reverseLatLong']);
        Route::post('/create', [AddressUserController::class, 'create']);
        Route::post('/update/{address:id}', [AddressUserController::class, 'update']);
        Route::post('/update-default/{address:id}', [AddressUserController::class, 'updateDefault']);
        Route::delete('/{address:id}', [AddressUserController::class, 'delete']);
    });

    Route::group(['prefix' => 'pre-order'], function () {
        Route::get('/', [PreOrderUserController::class, 'list']);
        Route::get('/{preorder:slug}', [PreOrderUserController::class, 'getBySlug']);
    });

    Route::group(['prefix' => 'cart', 'middleware' => ['auth:sanctum']], function () {
        Route::get('/', [CartUserController::class, 'list']);
        Route::get('checkout', [CartUserController::class, 'listCheckout']);
        Route::get('checkout/config', [CartUserController::class, 'listConfigCheckout']);
        Route::get('variant-item', [CartUserController::class, 'getCartVariantItem']);
        Route::post('create', [CartUserController::class, 'create']);
        Route::delete('/{cart:id}', [CartUserController::class, 'delete']);
    });

    Route::group(['prefix' => 'order', 'middleware' => ['auth:sanctum']], function () {
        Route::get('/', [OrderUserController::class, 'list']);
        Route::get('detail', [OrderUserController::class, 'getDetail']);
        Route::post('create', [OrderUserController::class, 'create']);
        Route::post('update-payment/{order:id}', [OrderUserController::class, 'updatePayment']);
        Route::post('update-reject/{order:id}', [OrderUserController::class, 'updateReject']);
    });
});
