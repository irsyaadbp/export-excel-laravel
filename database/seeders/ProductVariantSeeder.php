<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductVariantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * $table->string('name')->unique();
         * $table->string('photo_url')->nullable();
         * $table->string('description')->nullable();
         */

        DB::table('product_variants')->insert([
            'name' => 'Size',
            'description' => 'This is description for Size',
        ]);
        DB::table('product_variants')->insert([
            'name' => 'Flavor',
            'description' => 'This is description for Flavor',
        ]);
    }
}
