<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'meta_key' => 'menu-1_title',
                'meta_value' => 'Home',
            ],
            [
                'meta_key' => 'menu-1_link',
                'meta_value' => '/',
            ],
            [
                'meta_key' => 'menu-1_active',
                'meta_value' => '1',
            ],
            [
                'meta_key' => 'menu-2_title',
                'meta_value' => 'Booking A Slot',
            ],
            [
                'meta_key' => 'menu-2_link',
                'meta_value' => '/page/booking-a-slot',
            ],
            [
                'meta_key' => 'menu-2_active',
                'meta_value' => '1',
            ],
            [
                'meta_key' => 'menu-3_title',
                'meta_value' => 'FAQ',
            ],
            [
                'meta_key' => 'menu-3_link',
                'meta_value' => '/page/link',
            ],
            [
                'meta_key' => 'menu-3_active',
                'meta_value' => '1',
            ],
            [
                'meta_key' => 'menu-4_title',
                'meta_value' => 'Contact Us',
            ],
            [
                'meta_key' => 'menu-4_link',
                'meta_value' => '/page/contact-us',
            ],
            [
                'meta_key' => 'menu-4_active',
                'meta_value' => '1',
            ],
            [
                'meta_key' => 'menu-5_title',
                'meta_value' => 'About',
            ],
            [
                'meta_key' => 'menu-5_link',
                'meta_value' => '/page/about',
            ],
            [
                'meta_key' => 'menu-5_active',
                'meta_value' => '1',
            ],
            [
                'meta_key' => 'menu-6_title',
                'meta_value' => 'Contact Us',
            ],
            [
                'meta_key' => 'menu-6_link',
                'meta_value' => '/',
            ],
            [
                'meta_key' => 'menu-6_active',
                'meta_value' => '0',
            ],
            [
                'meta_key' => 'config_delivery_fee_kilometer',
                'meta_value' => '10000',
            ],
            [
                'meta_key' => 'config_delivery_max_distance',
                'meta_value' => '10',
            ],
            [
                'meta_key' => 'config_latitude_shop',
                'meta_value' => '-7.1428148',
            ],
            [
                'meta_key' => 'config_longitude_shop',
                'meta_value' => '112.5843609',
            ],
            [
                'meta_key' => 'config_pickup_max_time_start',
                'meta_value' => "12:00",
            ],

            [
                'meta_key' => 'config_pickup_max_time_end',
                'meta_value' => "17:00",
            ],
            [
                'meta_key' => 'config_delivery_max_time_start',
                'meta_value' => "12:00",
            ],
            [
                'meta_key' => 'config_delivery_max_time_end',
                'meta_value' => "17:00",
            ],
            [
                'meta_key' => 'config_email_receiver',
                'meta_value' => 'support@mooncena.com',
            ],
            [
                'meta_key' => 'config_no_wa',
                'meta_value' => '62839128312313',
            ],
            [
                'meta_key' => 'config_logo_light',
                'meta_value' => null,
            ],
            [
                'meta_key' => 'config_logo_dark',
                'meta_value' => null,
            ],
            [
                'meta_key' => 'config_logo_icon',
                'meta_value' => null,
            ],
        ];


        foreach ($data as $value) {
            DB::table('configs')->insert($value);
        }
    }
}
