<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $table->foreignId('user_id')->constrained()->onDelete('cascade');
        // $table->string('label')->unique();
        // $table->string('name');
        // $table->decimal('lat', 8, 6);
        // $table->decimal('long', 9, 6);
        // $table->string('phone');
        // $table->string('address');
        // $table->string('note')->nullable();
        // $table->tinyInteger('default')->default(0);
        //

        $data = [
            [
                'user_id' => 1,
                'name' => 'Irsyaad Budi',
                'label' => 'Rumah',
                'lat' => -7.803326358921882,
                'long' => 110.30864624023276,
                'phone' => '089723424234',
                'address' => 'Jalan Wates',
                'note' => 'rumah warna ungu',
                'default' => 1,
            ],
            [
                'user_id' => 1,
                'name' => 'Irsyaad Budi',
                'label' => 'Kantor',
                'lat' => -7.786625,
                'long' => 110.321716,
                'phone' => '089723424234',
                'address' => 'Ambarketawang, Gamping',
                'note' => 'kasih ke pak satpam aja',
                'default' => 0,
            ]
        ];

        DB::table('addresses')->insert($data);
    }
}
