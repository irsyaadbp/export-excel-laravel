<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         *  $table->string('name')->unique();
         *  $table->string('photo_url')->nullable();
         *  $table->string('description')->nullable();
         *  $table->decimal('price', 24, 2);
         */
        DB::table('products')->insert([
            'name' => 'Bento Cake',
            'photo_url' => 'https://loremflickr.com/400/400/food',
            'description' => 'This is description for Bento Cake',
            'price' => 10000,
        ]);
        DB::table('products')->insert([
            'name' => 'Spons Cake',
            'photo_url' => 'https://loremflickr.com/400/400/food',
            'description' => 'This is description for Spons Cake',
            'price' => 20000,
        ]);
    }
}
