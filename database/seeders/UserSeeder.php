<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create user role admin and user
        DB::table('users')->insert([
            'first_name' => 'Admin',
            'last_name' => 'Mooncena',
            'email' => 'setanalasan48@gmail.com',
            'role' => 'admin',
            'password' => Hash::make('!Qwerty123'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Irsyaad',
            'last_name' => 'Budi',
            'email' => 'infonesia08@gmail.com',
            'role' => 'user',
            'password' => Hash::make('!Qwerty123'),
        ]);
    }
}
