<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VariantItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * $table->string('name');
         * $table->string('photo_url')->nullable();
         * $table->foreignId('product_id')->constrained();
         * $table->foreignId('product_variant_id')->constrained();
         * $table->decimal('extra_price', 24, 2);
         */

        //  Seeder for Size for bento cake
        DB::table('variant_items')->insert([
            'name' => 'Large',
            'product_id' => 1,
            'product_variant_id' => 1,
            'extra_price' => 5000,
        ]);
        DB::table('variant_items')->insert([
            'name' => 'Regular',
            'product_id' => 1,
            'product_variant_id' => 1,
            'extra_price' => 0,
        ]);

        //  Seeder for Size for spons cake
        DB::table('variant_items')->insert([
            'name' => 'Large',
            'product_id' => 2,
            'product_variant_id' => 1,
            'extra_price' => 5000,
        ]);
        DB::table('variant_items')->insert([
            'name' => 'Regular',
            'product_id' => 2,
            'product_variant_id' => 1,
            'extra_price' => 0,
        ]);

        //  Seeder for Flafor
        DB::table('variant_items')->insert([
            'name' => 'Coklat',
            'product_id' => 1,
            'product_variant_id' => 2,
            'extra_price' => 0,
        ]);
        DB::table('variant_items')->insert([
            'name' => 'Strawberry',
            'product_id' => 1,
            'product_variant_id' => 2,
            'extra_price' => 0,
        ]);
        DB::table('variant_items')->insert([
            'name' => 'Vanilla',
            'product_id' => 1,
            'product_variant_id' => 2,
            'extra_price' => 0,
        ]);
    }
}
