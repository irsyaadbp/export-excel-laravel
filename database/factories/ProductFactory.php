<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->word(),
            'photo_url' => 'https://loremflickr.com/400/400/food',
            'description' => $this->faker->sentence(),
            'price' => $this->faker->numberBetween($min = 1500, $max = 100000)
        ];
    }
}
