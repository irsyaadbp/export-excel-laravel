<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreOrderMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_order_metas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pre_order_id')->constrained()->onDelete('cascade');
            $table->string('meta_key'); // allow_delivery_type, 
            $table->longText('meta_value')->nullable(); // delivery, pickup, ojek online
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_order_metas');
    }
}
